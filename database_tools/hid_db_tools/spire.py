'''
Created on Sep 22, 2015

@author: bhasnoun
'''

import tools
import pyfits, os, numpy, warnings, time


def get_all_metadata_spire(fitsfile):
    
    metadata = tools.get_all_metadata(fitsfile)
    
    filename = metadata["FILENAME"].lower()

    if metadata["INSTRUMENT"].lower()=="maps_spire":
        
        if len(numpy.unique([filename.count(i) for i in ["extemigainsapplied","destriped","supreme","psrc","extdzeropoint","combined"]])) != 1:
            metadata["PRODUCT_LEVEL"] = "Level25"
            metadata["VO"] = True
        else:
            metadata["PRODUCT_LEVEL"] = "Level2"
            metadata["VO"] = False
        
    if metadata["FILEPATH"].lower().count("l1_sanepic_spire") > 0: 
        metadata["PRODUCT_LEVEL"] = "Level1"
        metadata["VO"] = False

    if metadata["INSTRUMENT"].lower()=="fts_spire":

        if filename.count("sdi")>0:             metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "sdi", "Level1"
        if filename.count("sdt")>0:             metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "sdt", "Level1"
        if filename.count("spectrum")>0:        metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Spectrum", "Level1"
        if filename.count("spectrum2d")>0:      metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Spectrum2d", "Level1"
        if filename.count("gridding_cube")>0:   metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Gridding Cube", "Level2"
        if filename.count("naive_cube")>0:      metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Naive Cube", "Level2"
        if filename.count("nearest_cube")>0:    metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Nearest Cube", "Level2"
        if filename.count("supreme")>0:         metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Supreme Cube", "Level2"
    
        if filename.count("apodized")>0: metadata["APODNAME"]="apod"
        else: metadata["APODNAME"]="unapod"
        
        metadata["VO"] = False
    
    metadata["PROCESSING"] = "HIPE"
    if (filename.count("_dbl") == 0 and filename.count("smpvt") == 0 and filename.count("fixed") == 0 and filename.count("test") == 0 and filename.count("_all") == 1 and filename.count("_extemigainsapplied") == 0):
        metadata["PROCESSING"] = "HIPE_Sanepic"
    if filename.count("supreme") > 0:
        metadata["PROCESSING"] = "Supreme Plugin"
    
    return metadata

def create_table_spire(conn, table):
    
    print "create table "+table
    
    cur = conn.cursor()
    
    if table == "spire_photo_l1":
        cur.execute("CREATE TABLE "+table+" (\
             LINE_ID SERIAL PRIMARY KEY,OBS_ID INTEGER[],OBJECT VARCHAR, OBSERVER VARCHAR,PROPOSAL VARCHAR,BAND VARCHAR,\
             BUILDVERSION VARCHAR, CALIBRATIONVERSION VARCHAR,\
             PUBLIC BOOLEAN,PROCESSING VARCHAR,LAST BOOLEAN,PRODUCT_LEVEL VARCHAR,VO BOOLEAN,\
             FILENAME VARCHAR,RELEASE VARCHAR,PROGRAM VARCHAR,FILEPATH VARCHAR,FILESIZE_BYTES REAL,FILESIZE REAL,METADATA JSON\
             )")
    
    if table == "spire_photo_l2":
        cur.execute("CREATE TABLE "+table+" (\
             LINE_ID SERIAL PRIMARY KEY,OBS_ID INTEGER[],OBJECT VARCHAR, OBSERVER VARCHAR,PROPOSAL VARCHAR,BAND VARCHAR,\
             BUILDVERSION VARCHAR, CALIBRATIONVERSION VARCHAR,\
             RA REAL,DEC REAL,XYZ_X REAL,XYZ_Y REAL,XYZ_Z REAL,SPOLY SPOLY,POLYGON GEOMETRY,\
             PUBLIC BOOLEAN,PROCESSING VARCHAR,LAST BOOLEAN,PRODUCT_LEVEL VARCHAR,VO BOOLEAN,\
             FILENAME VARCHAR,RELEASE VARCHAR,PROGRAM VARCHAR,FILEPATH VARCHAR,FILESIZE_BYTES REAL,FILESIZE REAL,METADATA JSON\
             )")
    
    if table == "spire_fts_l1":
        cur.execute("CREATE TABLE "+table+" (\
             LINE_ID SERIAL PRIMARY KEY,OBS_ID INTEGER[],OBJECT VARCHAR, OBSERVER VARCHAR,PROPOSAL VARCHAR,WAVELNTH VARCHAR,\
             BUILDVERSION VARCHAR, CALIBRATIONVERSION VARCHAR,\
             CMDRES VARCHAR,APODNAME VARCHAR,MAPSAMPL VARCHAR,PRODUCT VARCHAR,\
             PUBLIC BOOLEAN,PROCESSING VARCHAR,LAST BOOLEAN,PRODUCT_LEVEL VARCHAR,VO BOOLEAN,\
             FILENAME VARCHAR,RELEASE VARCHAR,PROGRAM VARCHAR,FILEPATH VARCHAR,FILESIZE_BYTES REAL,FILESIZE REAL,METADATA JSON\
             )")
     
    if table == "spire_fts_l2":
        cur.execute("CREATE TABLE "+table+" (\
             LINE_ID SERIAL PRIMARY KEY,OBS_ID INTEGER[],OBJECT VARCHAR, OBSERVER VARCHAR,PROPOSAL VARCHAR,WAVELNTH VARCHAR,\
             BUILDVERSION VARCHAR, CALIBRATIONVERSION VARCHAR,\
             RA REAL,DEC REAL,XYZ_X REAL,XYZ_Y REAL,XYZ_Z REAL,SPOLY SPOLY,POLYGON GEOMETRY,\
             CMDRES VARCHAR,APODNAME VARCHAR,MAPSAMPL VARCHAR,PRODUCT VARCHAR,\
             PUBLIC BOOLEAN,PROCESSING VARCHAR,LAST BOOLEAN,PRODUCT_LEVEL VARCHAR,VO BOOLEAN,\
             FILENAME VARCHAR,RELEASE VARCHAR,PROGRAM VARCHAR,FILEPATH VARCHAR,FILESIZE_BYTES REAL,FILESIZE REAL,METADATA JSON\
             )")
    
    if table == "spire_photo_density":
        cur.execute("create table spire_photo_density (L2_id serial PRIMARY KEY,obsid integer,object varchar,filepath varchar,observer varchar,\proposal varchar,program varchar,filename varchar,image varchar,\
            crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,\
            equinox varchar,description varchar,maxdens real,maxsig real,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,last boolean)")

    if table == "spire_catalog":
        cur.execute("create table spire_catalog (source_id serial PRIMARY KEY, ra real,dec real,x real,y real,raPlusErr real,decPlusErr real,raMinusErr real,decMinusErr real,xPlusErr real,yPlusErr real,xMinusErr real,yMinusErr real,\
            flux real,fluxPlusErr real,fluxMinusErr real,background real,bgPlusErr real,bgMinusErr real,quality real,object varchar,proposal varchar,band varchar,obsid integer)")
    
    
    cur.close()

def keys_table_spire(table):
    
    if table == "spire_photo_l1":
        return ['OBS_ID','OBJECT','OBSERVER','PROPOSAL','BAND',
               'BUILDVERSION','CALIBRATIONVERSION',
               'PUBLIC','PROCESSING','LAST','PRODUCT_LEVEL','VO',
               'FILENAME','RELEASE','PROGRAM','FILEPATH','FILESIZE_BYTES','FILESIZE','METADATA']

    if table == "spire_photo_l2":
        return ['OBS_ID','OBJECT','OBSERVER','PROPOSAL','BAND',
               'BUILDVERSION','CALIBRATIONVERSION',
               'RA','DEC','XYZ_X','XYZ_Y','XYZ_Z','SPOLY','POLYGON',
               'PUBLIC','PROCESSING','LAST','PRODUCT_LEVEL','VO',
               'FILENAME','RELEASE','PROGRAM','FILEPATH','FILESIZE_BYTES','FILESIZE','METADATA']

    if table == "spire_fts_l1":
        return ['OBS_ID','OBJECT','OBSERVER','PROPOSAL','WAVELNTH',
               'BUILDVERSION','CALIBRATIONVERSION',
               'CMDRES','APODNAME','MAPSAMPL','PRODUCT',
               'PUBLIC','PROCESSING','LAST','PRODUCT_LEVEL','VO',
               'FILENAME','RELEASE','PROGRAM','FILEPATH','FILESIZE_BYTES','FILESIZE','METADATA']
               
    if table == "spire_fts_l2":
        return ['OBS_ID','OBJECT','OBSERVER','PROPOSAL','WAVELNTH',
               'BUILDVERSION','CALIBRATIONVERSION',
               'RA','DEC','XYZ_X','XYZ_Y','XYZ_Z','SPOLY','POLYGON',
               'CMDRES','APODNAME','MAPSAMPL','PRODUCT',
               'PUBLIC','PROCESSING','LAST','PRODUCT_LEVEL','VO',
               'FILENAME','RELEASE','PROGRAM','FILEPATH','FILESIZE_BYTES','FILESIZE','METADATA']

def fill_spire_photo_catalog(listfile, table, conn):
    """Fill Spire Photo Catalog database"""
    
    cur = conn.cursor()
    
    for i in listfile:
        fits = pyfits.open(i, mode='update')
        allSources = fits[1].data
        for j in range(len(allSources)):
            source = list(allSources[j])
            cur.execute("select count (*) from " + table)
            nblines = cur.fetchone()
            if nblines is not None: source_id = nblines[0]
            else: source_id = 0
            source.insert(0, int(source_id))
            source[-1] = int(source[-1])  # obsid to int not int32
            source = tuple(source)
            print source
            cur.execute("insert into " + table + " values (" + (len(source) - 1) * "%s," + "%s)", source)
    
    cur.close()

def create_spire_pacs_l25_view(conn, release_spire, release_pacs):
    cur = conn.cursor()
    cur.execute("drop view spirepacs_photo_L25")
    cur.execute(
        "create view spirepacs_photo_l25  as select 'spire photometer' as instrument,object ,filepath ,observer ,program ,processing ,filename ,crval1 ,crval2 ,cdelt1 ,cdelt2 ,crota2 ,naxis1 ,naxis2 ,crpix1 ,crpix2 ,ctype1 ,ctype2 ,equinox ,wavelength*1E-6 as \"wavelength\" ,description ,BuildVersion ,X ,Y ,Z ,filesize_bytes ,filesize ,release ,last ,spoly from spire_photo_l2 where level25='true' and (program='SAG-4' or program='SAG-3' or program='clusters_lowz') and release like '" + release_spire + "' UNION ALL SELECT 'pacs photometer' as instrument,object ,filepath ,observer ,program ,processing ,filename ,crval1 ,crval2 ,cdelt1 ,cdelt2 ,crota2 ,naxis1 ,naxis2 ,crpix1 ,crpix2 ,ctype1 ,ctype2 ,equinox ,wavelength ,description ,BuildVersion ,X ,Y ,Z ,filesize_bytes ,filesize ,release ,last ,spoly from pacs_photo_l2 where (program='SAG-4' or program='SAG-3' ) and release like '" + release_pacs + "'")
    
    cur.close()


#alter table spire_photo_l1 drop column l1_id;
#alter table spire_photo_l1 add column l1_id serial;
#alter table spire_photo_l1 add primary key (l1_id);
