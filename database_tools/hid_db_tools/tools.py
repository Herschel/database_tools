'''
Created on Sep 22, 2015

@author: bhasnoun
'''

import pyfits, os, numpy, warnings, time

def get_connection(mode):
    '''
    This function connect to the type of database in which you want to ingest your metadata.
    It returns the connection.
    
    :param mode: type of database, "postgres", "mongodb", "pandas"
    :return connection: connection
    '''

    if mode == "postgres":
        import psycopg2
        try:
            conn = psycopg2.connect("dbname="'HID_glx'" user='sitools'")
        except:
            print "I am unable to connect to the PostgreSQL database"
            sys.exit()
    if mode == "mongodb":
        import pymongo
        try:
            conn = pymongo.MongoClient('localhost', 27017)
        except:
            print "I am unable to connect to the MongoDB database"
            sys.exit()

    if mode == "panda":
        conn = None

    return conn

def close_connection(mode,conn):
    '''
    This function close the connection.
    For "postgres" mode, a commit is done before closing the connection.
    
    :param mode: type of database, "postgres", "mongodb", "pandas"
    :param conn: connection to close
    '''
    
    if mode == "postgres":
        conn.commit()
        conn.close()

    if mode == "mongodb":
        conn.close()

def get_listfile_to_inject(table,directory):
    '''
    This function gets all the fits files from which you want to extract the metadata.
    It returns a list of fits files
    
    :param table: database table where to put the extracted metadata
    :param directory: directory to scan for fits files
    :return listfiles: list of fits files
    '''
    
    #directory = "/Users/bhasnoun/Documents/Work/"

    if table == "spire_photo_l1":
        pattern = ["L1_Sanepic_SPIRE"]
        exclude = []

    if table == "spire_photo_l2":
        pattern = ["MAPS_SPIRE"]
        exclude = ["CoAdd","color","density","HiRes","old","test","_notdf"]

    if table == "spire_photo_density":
        pattern = ["colorsel"]
        exclude = []

    if table == "spire_catalog":
        pattern = ["catalog","mask","beam"]
        exclude = []

    if table == "spire_fts_l1":
        pattern = []
        exclude = ["cube","extra"]

    if table == "spire_fts_l2":
        pattern = ["cube.fits"]
        exclude = ["extra"]

    if table == "pacs_spectro_l1":
        pattern = ["L1"]
        exclude = ["_flag","Frame","_ProjectedCube","PACSman","MapFlux"]

    if table == "pacs_spectro_l2":
        pattern = ["Cube.fits","L2"]
        exclude = ["MapFlux","PACSman","SAG-4/HD37041","RebinnedCube","RebinnedCubeAfterSub","RasterCube","RasterCubeBFF","Frame"]

    if table == "pacs_spectro_l25":
        pattern = ["PACSman"]
        exclude = ["RasterCubeCorrPACSman","mask","smooth","spectra"]

    if table == "pacs_photo_l1":
        pattern = ["L1_PACS"]
        exclude = ["old","test"]

    if table == "pacs_photo_l2":
        pattern = ["MAPS_PACS"]
        exclude = ["old","test"]

    if table != "spirepacs_photo_l25":

        listfile = get_files_pattern(directory,pattern,exclude)
        print len(listfile),"files to add to table",table
        return listfile[:]
    else:
        return []

def update_progress(ratioDone,message=""):
    """
    This function show a progression bar.
    
    :param ratioDone: ratio done. number in [0,1]
    :param message: message to show next to the progession bar
    """
    
    import sys
    increment = 50
    done = int(ratioDone*increment)
    left = increment - done
    sys.stdout.write("\r["+"="*done+" "*left+"] "+str(int(ratioDone*100))+"% "+message)
    sys.stdout.flush()
    #if ratioDone==1: print ""

def get_values_from_keys(keys,metadata):
    """
    This function get all the values from a list of keys
    
    :param keys: list of keys
    :param metadata: metadata dictionary from which to extract values
    :return values: list of values
    """
    
    import json
    values = []
    for key in keys:
        if key=="METADATA": values.append(json.dumps(metadata,sort_keys=True))
        else:
            try: values.append(metadata[key])
            except: values.append(0)
    
    return values

def get_file_info(fitsfile):
    """
    This function get some file information concerning the path of a fits file
    
    :param fitsfile: fits file to analyse
    :return metadata: dictionary of information
    """

    metadata = {}
    
    filename = os.path.basename(fitsfile)
    filepath = fitsfile.split("/Release")[1]
    filepath = correct_symbols_in_filepath(filepath)
    
    metadata["RELEASE"]         = fitsfile.split("/")[-6]
    metadata["FITS_FROM"]       = fitsfile.split("/")[-5]
    metadata["INSTRUMENT"]      = fitsfile.split("/")[-4]
    metadata["PROGRAM"]         = fitsfile.split("/")[-3]
    metadata["FIELD"]           = fitsfile.split("/")[-2]
    metadata["FILENAME"]        = filename
    metadata["FILEPATH"]        = filepath
    metadata["IMAGEPATH"]       = filepath[:len(filepath)-5]+".png"
    metadata["FULLIMAGEPATH"]   = filepath[:len(filepath)-5]+"_fullsize.png"
    metadata["FILESIZE_BYTES"]  = float(os.path.getsize(fitsfile))
    metadata["FILESIZE"]        = convert_megabytes(os.path.getsize(fitsfile))
    
    return metadata

def get_metadata_from_header(header,process_comments=False):
    """
    This function get all the metadata from a fits file header.
    It returns a metadata dictionary conataining all the key, values.
    
    :param header: fits file header
    :param process_comments: boolean to also process multiple lines comments
    :return: dictionary of metadata
    """
    
    # Process the comments of each card
    # Backward to agregate comments on multiple lines
    if process_comments:
        cards = [ [card[0],card[1],card[2]] for card in header.cards ]
        temp_com = ''
        for i in range(len(cards))[::-1]: 
            if cards[i][0] == 'COMMENT': 
                temp_com = cards[i][1] + temp_com
            if cards[i][0] != 'COMMENT' and temp_com != '':
                cards[i][2] = (cards[i][2] + temp_com).replace('&','')
                temp_com = ''
    
    else: cards = [ [card[0],card[1]] for card in header.cards ]
    
    # create a dictionnary of the META keywords
    dic = {}
    for card in cards:
        if card[0].count('key.META')>0: dic.update({card[0].replace('key.',''):card[1].upper()})
    
    # filter all COMMENT, empty and HIERARCH keywords
    cards = [ card for card in cards if (card[0] != 'COMMENT') and (card[0] != '') and (card[0].count('key.')==0)]
    
    # make final metadata dictionary
    metadata = {}
    for card in cards:
        if str(card[1]).count("'")>0: card[1] = str(card[1]).replace("'","")
        if str(card[1]).count('"')>0: card[1] = str(card[1]).replace('"','')
        if card[0] in dic.keys(): metadata.update({dic[card[0]]:card[1]})
        else: metadata.update({card[0]:card[1]})
    
    return metadata

def get_all_metadata(fitsfile):
    """
    This function get all the metadata from the two first header and an eventual obsids extension from a fits file
    
    :param fitsfile: fits file to analyse
    :return metadata: dictionary of metadata related to the header and the file path.
    """
    
    warnings.filterwarnings('ignore')
    
    metadata = get_file_info(fitsfile)
    
    fits = pyfits.open(fitsfile)
    for i in [0,1]: 
        hdu = fits[i]
        hdu.verify('fix')
        metadata.update(get_metadata_from_header(hdu.header))
    
    try:
        try: 
            obsids = []
            for obsid in fits['obsids'].data: obsids.append(obsid[0])
            metadata["OBS_ID"] = "{"+",".join([ str(int(obsid)) for obsid in obsids ])+"}"
            metadata["COMBINED"] = True
        except:
            metadata["OBS_ID"] = "{"+str(int(metadata["OBS_ID"]))+"}"
            metadata["COMBINED"] = False
    except:
        metadata["OBS_ID"] = "{1111111111}"
        metadata["COMBINED"] = False
    
    try:
        metadata["RA"] = metadata["CRVAL1"]
        metadata["DEC"] = metadata["CRVAL2"]
    except:
        pass
    
    try: metadata["BUILDVERSION"] = fits["HistoryTasks"].data[-1][-1]
    except: metadata["BUILDVERSION"] = "N/A"

    fits.close()

    try: metadata["PUBLIC"] = is_public(metadata["DATE-OBS"])
    except: metadata["PUBLIC"] = False

    try: metadata['XYZ_X'], metadata['XYZ_Y'], metadata['XYZ_Z'] = ra_dec_to_xyz(float(metadata['RA']), float(metadata['DEC']))
    except: metadata['XYZ_X'], metadata['XYZ_Y'], metadata['XYZ_Z'] = 0, 0, 0
    
    metadata["SPOLY"] = calculate_spoly(fitsfile)
    
    metadata["POLYGON"] = calculate_poly(fitsfile)
    
    try:
        if metadata["ORIGIN"].count("LAM") > 0: metadata["ORIGIN"] = "LAM"
        else: metadata["ORIGIN"] = "IAS"
    except:
        metadata["ORIGIN"] = "IAS"

    return metadata

def check_pattern(string, patterns, exclude):
    """
    This function check if a string contains some patterns and not some other
    
    :param string: string to analyse
    :param pattern: patterns to be in the string analysed
    :param exclude: patterns to not be in the string analysed
    :return is_public: boolean 
    """
    
    for pattern in patterns:
        if string.count(pattern)==0: return False
    for excluded in exclude:
        if string.count(excluded)>0: return False
    
    return True

def get_files_pattern(basepath,pattern=[],exclude=[],sort=True):
    """
    This function get all the files from a directory, subdirectories included, that have some patterns and not others
    
    :param basepath: directory to scan
    :param pattern: patterns to be in the string analysed
    :param exclude: patterns to not be in the string analysed
    :param sort: sorting the fits files or not
    :return listfile: list of fits files
    """
    
    listfile = []
    for root, dirs, files in os.walk(basepath):
        listfile.extend( [root+"/"+filename for filename in files if filename.endswith(".fits") and check_pattern(root+"/"+filename,pattern,exclude)] )
    if sort: listfile = sorted(listfile)
    
    return listfile

def is_public(timeObs):
    """
    This function check if an observation is public (more than one year old).
    """
    
    from datetime import date
    
    today = date.today()
    
    timeObsSplit = timeObs.split("-")
    year = timeObsSplit[0]
    month = timeObsSplit[1]
    timeObsSplit2 = timeObsSplit[2].split("T")
    day = timeObsSplit2[0]
    dateTimeObs = date(int(year),int(month),int(day))
    deltaTime = abs(dateTimeObs-today)
    
    if deltaTime.days<365: return False
    
    return True

def delete_release(table,release,mode):
    """
    This funcion delete a release from the database
    
    :param conn: connection to the database
    :param table: table to clean
    :param release: name of the release to delete from the table
    """
    
    if mode == "postgres":
        conn = get_connection(mode)
        cur = conn.cursor()
        #test Release existence
        test_release_request = "select count (*) from "+table+" where release='"+release+"'"
        cur.execute(test_release_request)
        if cur.fetchone() is not None:
            delete_request = "delete from "+table+" where release='"+release+"'"
            cur.execute(delete_request)
        cur.close()
        close_connection(mode,conn)

def drop_table(table, mode):
    """
    This funcion drop a table from the database
    
    :param table: table to drop
    :param mode: 
    """
    
    conn = get_connection(mode)
    
    if mode == "postgres":
        
        cur = conn.cursor()
        command = "select tablename from pg_tables where tablename like '"+table+"'"
        cur.execute(command)
        if cur.fetchone() is not None:
            print "drop table",table
            cur.execute("drop table %s" % table)
        cur.close()
    
    if mode == "mongodb":
        if table.count("spire")>0: collection = conn["herschel"]["spire"]
        if table.count("pacs")>0: collection = conn["herschel"]["pacs"]
        collection.drop()
        
    close_connection(mode,conn)

def ra_dec_to_xyz(ra, dec):
    """
    This funcion convert ra dec pointing to x y z
    
    :param ra: right ascension
    :param dec: declinaison
    :return x, y, z: cartesian x, y, z
    """
    
    import math
    try:
        ra_rad = math.radians(ra)
        dec_rad = math.radians(dec)
        x = math.cos(dec_rad)*math.cos(ra_rad)
        y = math.cos(dec_rad)*math.sin(ra_rad)
        z = math.sin(dec_rad)
        return x, y, z
    except:
        return 0., 0., 0.

def convert_megabytes(nb_bytes):
    """
    This function
    """
    
    nb_bytes = float(nb_bytes)
    megabytes = nb_bytes / 1048576
    return round(megabytes,2) 

def convert_bytes(nb_bytes):
    """
    This function
    """
    
    nb_bytes = float(nb_bytes)
    if nb_bytes >= 1099511627776:
        terabytes = nb_bytes / 1099511627776
        size = '%.2fT' % terabytes
    elif nb_bytes >= 1073741824:
        gigabytes = nb_bytes / 1073741824
        size = '%.2fG' % gigabytes
    elif bytes >= 1048576:
        megabytes = nb_bytes / 1048576
        size = '%.2fM' % megabytes
    elif nb_bytes >= 1024:
        kilobytes = nb_bytes / 1024
        size = '%.2fK' % kilobytes
    else:
        size = '%.2fb' % nb_bytes

    return size

def correct_symbols_in_filepath(filepath):
    """
    This function check if some special symbol are present in a file path and replace them.
    
    :param filepath: file path to correct
    :return filepath: file path corrected
    """
    
    dicURL = {}
    dicURL[" "] = "%20"
    dicURL[";"] = "%3b"
    dicURL["?"] = "%3f"
    dicURL[":"] = "%3a"
    dicURL["="] = "%3d"
    dicURL["+"] = "%2b"
    
    if len(numpy.unique([filepath.count(i) for i in dicURL.keys()])) != 1:
        for i in dicURL.keys(): filepath = filepath.replace(i,dicURL[i])

    return filepath

def calculate_spoly(filepath):
    """
    This function calculate spoly from a fits file.
    
    :param filepath: fits file
    :return spoly: polygone spoly
    """
    
    import pywcs
    fits = pyfits.open(filepath)
    try:
        if filepath.count("SCANA")==0:
            hdr1 = fits[1].header
            hdr1['CUNIT1'], hdr1['CUNIT2'] = "deg", "deg"
            wcs = pywcs.WCS(hdr1)
        else:
            ###### for SCANAMORPHOS fits files (3D)
            wcs = pywcs.WCS(fits['Primary'].header)
            hdr1 = fits['PrimaryImage'].header

    except KeyError:
        print "EE - No 'Image' extension in "+os.path.basename(filepath)
        return 2
    
    try:
        point1 = wcs.wcs_pix2sky([[0.5,0.5]],0)
        point2 = wcs.wcs_pix2sky([[0.5+hdr1['NAXIS1'],0.5]],0)
        point3 = wcs.wcs_pix2sky([[0.5+hdr1['NAXIS1'],0.5+hdr1['NAXIS2']]],0)
        point4 = wcs.wcs_pix2sky([[0.5,0.5+hdr1['NAXIS2']]],0)
        point1 = "("+str(point1[0,0])+"d,"+str(point1[0,1])+"d)"
        point2 = "("+str(point2[0,0])+"d,"+str(point2[0,1])+"d)"
        point3 = "("+str(point3[0,0])+"d,"+str(point3[0,1])+"d)"
        point4 = "("+str(point4[0,0])+"d,"+str(point4[0,1])+"d)"
        
        spoly = "{"+point4+","+point3+","+point2+","+point1+"}"
        
        return spoly
    
    except:
        return "{(0d , 10d),(10d , 0d),(0d , 0d)}"

def calculate_poly(filepath):
    """
    This function calculate spoly from a fits file.
    
    :param filepath: fits file
    :return spoly: polygone spoly
    """
    
    import pywcs
    
    fits = pyfits.open(filepath)
    try:
        if filepath.count("SCANA")==0:
            hdr1 = fits[1].header
            hdr1['CUNIT1'], hdr1['CUNIT2'] = "deg", "deg"
            wcs = pywcs.WCS(hdr1)
        else:
            ###### for SCANAMORPHOS fits files (3D)
            wcs = pywcs.WCS(fits['Primary'].header)
            hdr1 = fits['PrimaryImage'].header

    except KeyError:
        print "EE - No 'Image' extension in "+os.path.basename(filepath)
        return 2
    
    try:
        point1 = wcs.wcs_pix2sky([[0.5,0.5]],0)
        point2 = wcs.wcs_pix2sky([[0.5+hdr1['NAXIS1'],0.5]],0)
        point3 = wcs.wcs_pix2sky([[0.5+hdr1['NAXIS1'],0.5+hdr1['NAXIS2']]],0)
        point4 = wcs.wcs_pix2sky([[0.5,0.5+hdr1['NAXIS2']]],0)
        
        poly = 'POLYGON((%s %s, %s %s, %s %s, %s %s, %s %s))' % (point4[0,0],point4[0,1],point3[0,0],point3[0,1],point2[0,0],point2[0,1],point1[0,0],point1[0,1],point4[0,0],point4[0,1])
        return poly
    
    except:
        
        poly = 'POLYGON((%s %s, %s %s, %s %s))' % (0,0,0,0,0,0)
        return poly

def create_index_spoly(listTable,cur):
    """
    This function
    """
    
    for table in listTable:
        command="CREATE UNIQUE INDEX spoly_"+table+"_idx ON "+table+" (spoly);"
        print command
        cur.execute(command)
    return cur
