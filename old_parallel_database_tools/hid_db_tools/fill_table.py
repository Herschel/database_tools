'''
Created on Sep 28, 2015

@author: bhasnoun
'''

import pyfits, os, numpy, warnings, time
import tools, spire, pacs

def get_metadata_instru(table,fitsfile):
    """
    This function get the needed metadata from a fits file depending on a table.
    
    :param table: table where the metadata will go
    :param fitsfile: fits file to process
    :return metadata: key, values metadata dictionary
    """
    
    if table.count("spire")>0: metadata = spire.get_all_metadata_spire(fitsfile)
    if table.count("pacs_photo")>0: metadata = pacs.get_all_metadata_pacs_photo(fitsfile)
    if table.count("pacs_spectro")>0:
        if table == "pacs_spectro_l25": metadata = pacs.get_all_metadata_pacs_spectro_l25(fitsfile)
        else: metadata = pacs.get_all_metadata_pacs_spectro(fitsfile)
    
    return metadata

def create_table(table,mode):
    """
    This function create a table in the database with the corresponding column.
    
    :param conn: connection to use
    :param table: table to create
    """
    
    if mode == "postgres":
        tools.drop_table(table,mode)
        conn = tools.get_connection(mode)
        if table.count("spire")>0: spire.table_spire(table,conn)
        if table.count("pacs")>0:  pacs.table_pacs(table,conn)
        tools.close_connection(mode,conn)
    
    if mode == 'mongodb':
        tools.drop_table(table,mode)

def keys_table(table,conn=None,col=False):
    """
    This function return the keys to use to insert in a table.
    
    :param table: table from which the keys
    :return keys: list of keys
    """
    
    if table.count("spire")>0: keys = spire.table_spire(table,conn,col)
    if table.count("pacs")>0:  keys = pacs.table_pacs(table,conn,col)
    
    return keys

def fill_table_parallel(listfile,table,last,mode="panda",verbose=False):
    """
    This function fill a table with all the metadata from a list of fits files.
    The metadata gathering will be done using multiprocessing.
    
    :param listfile: list of list files
    :param table: table to  fill
    :param conn: connection to use
    :param last: name of the last release
    :param mode: database mode. "postgres", "mongodb" or "pandas"
    """
    
    conn = tools.get_connection(mode)
    
    from multiprocessing import Pool, Manager
    
    pool = Pool()
    manager = Manager()
    queue = manager.Queue()

    t = time.time()

    args = [ (queue, i, table, last, mode, verbose) for i in listfile ]

    results = pool.map_async(get_metadata_for_table_star,args)

    while True:
        done = "{:.2f}".format( 100.0 *queue.qsize() / float(len(listfile)) )
        done = queue.qsize() / float(len(listfile))
        if results.ready():
            if not verbose: tools.update_progress(done,table)
            break
        else:
            if not verbose: tools.update_progress(done,table)
            time.sleep(0.1)

    all_metadata = results.get()

    if mode == "postgres":
        keys = keys_table(table)
        all_column = str(tuple(keys)).replace("'","")
        all_values = ",".join( str(tuple(tools.get_values_from_keys(keys,metadata))) for metadata in all_metadata )
        insert_command = "insert into " + table + " "+ all_column +" values " + all_values
        cur = conn.cursor()
        cur.execute(insert_command)
        cur.close()

    if mode == "mongodb":
        if table.count("spire")>0: collection = conn["herschel"]["spire"]
        if table.count("pacs")>0: collection = conn["herschel"]["pacs"]
        collection.insert_many(all_metadata)

    if mode == "panda":
        import pandas as pd
        dataframe_data = []
        keys = [ key for key in keys_table(table) in key != "METADATA" ]
        for metadata in all_metadata:
            metadata_values = tools.get_values_from_keys(keys,metadata)
            dataframe_data.append(tuple(metadata_values))
        dataframe = pd.DataFrame(data=dataframe_data, columns=keys)
        release = metadata["RELEASE"]
        dataframe.to_csv(release[:2]+"_"+table+".csv", index=True, header=True)

    tools.update_progress(1,table + " - {:.2f}".format(time.time()-t) + "s elapsed")
    print ""
    
    tools.close_connection(mode,conn)
    
    #update_vo(object_update_vo,table,conn,release,mode)

def get_metadata_for_table_star(args):
    """
    This function convert the `fill_table_one([a,b])` call to a `fill_table_one(a,b)` call.
    Necessary when using map_async using several parameters.
    """
    
    queue, arg = args[0], args[1:]
    done = get_metadata_for_table(*arg)
    queue.put([1])
    
    return done

def get_metadata_for_table(fitsfile,table,last,mode="panda",verbose=True):
    """
    This function get all the metadata need for insertion in the table.
    
    :param listfile: list of list files
    :param table: table to  fill
    :param last: name of the last release
    :param mode: database mode. "postgres", "mongodb" or "pandas"
    :return metadata: metadata dictionary
    """
    
    metadata = get_metadata_instru(table,fitsfile)
    release = metadata["RELEASE"]
    metadata.update({"LAST":release==last})
    
    if mode == "panda": metadata.pop("METADATA",None)
    if mode == "mongo": metadata.pop("METADATA",None)

    return metadata

def update_vo(objects,table,conn,release,mode="panda"):
    """
    
    """
    if mode == "panda":
        import pandas as pd
        dataframe = pd.read_csv(release[:2]+"_"+table+".csv")
        csv_obsid = dataframe.OBS_ID.get_values()

    if mode == "postgres": cur = conn.cursor()

    objects = numpy.unique(objects)
    for object in objects:
        if mode == "postgres":
            cur.execute("select count(*) from "+table+" where object = '"+object+"' and vo = true and release= '"+release+"';")
            nb_lines = cur.fetchone()
            if nb_lines is None: cur.execute("update "+table+" set vo = true where object = '"+object+"' and release= '"+release+"';")

        if mode == "panda":
            change_lines = numpy.where(csv_obsid==obsid)[0]
            dataframe.LEVEL25[change_lines] = False

    if mode == "panda":
        dataframe.to_csv(release[:2]+"_"+table+"_updatedLevel25.csv", index=True, header=True)

def add_to_database(table,release,directory,mode):
    """
    This function add to the database the metadata from all fits files in a directory.
    
    :param table: table to  fill
    :param release: name of the release
    :param directory: directory to scan
    :param mode: database mode. "postgres", "mongodb" or "pandas"
    """
    
    tools.delete_release(table,release,mode)
    listfile = tools.get_listfile_to_inject(table,directory)
    fill_table_parallel(listfile,table,last,mode,verbose=False)

if __name__ == "__main__":

    import sys

    try:
        table = sys.argv[1]
        release = sys.argv[2]
        directory = sys.argv[3]
        mode = sys.argv[4]
        print "Add to database:",sys.argv[1:]
        add_to_database(table,release,directory,mode)
    except:
        print "Input Error"
        pass
