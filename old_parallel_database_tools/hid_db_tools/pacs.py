'''
Created on Sep 23, 2015

@author: bhasnoun
'''

import tools
import pyfits, os, numpy, warnings, time


def get_all_metadata_pacs_photo(fitsfile):

    metadata = tools.get_all_metadata(fitsfile)

    filename = metadata["FILENAME"].lower()

    if filename.count("scana")>0:   metadata["PROCESSING"], metadata["PROCESSING_L1"], metadata["PRODUCT_LEVEL"] = "SCANAMORPHOS", "HIPE_Scanamorphos", "Level25"
    if filename.count("madmap")>0:  metadata["PROCESSING"], metadata["PROCESSING_L1"], metadata["PRODUCT_LEVEL"] = "HIPE MadMap", "HIPE_Scanamorphos", "Level2"
    if filename.count("naive")>0:   metadata["PROCESSING"], metadata["PROCESSING_L1"], metadata["PRODUCT_LEVEL"] = "HIPE Naive", "HIPE_Scanamorphos", "Level2"
    if filename.count("unimap")>0:  metadata["PROCESSING"], metadata["PROCESSING_L1"], metadata["PRODUCT_LEVEL"] = "UNIMAP", "HIPE_Scanamorphos", "Level25"
    if filename.count("hipe")>0:    metadata["PROCESSING"], metadata["PROCESSING_L1"], metadata["PRODUCT_LEVEL"] = "HIPE", "HIPE_Scanamorphos", "Level2"
    if filename.count("sanepic")>0: metadata["PROCESSING"], metadata["PROCESSING_L1"], metadata["PRODUCT_LEVEL"] = "SANEPIC", "HIPE_Sanepic", "Level25"
    
    camera = {160.0:"red",100.0:"green",70.0:"blue"}
    try:
        metadata['CAMERA'] = camera[float(metadata['WAVELNTH'])]
        metadata['WAVELNTH'] = float(metadata['WAVELNTH'])*1E-06
    except:
        try:
            metadata['CAMERA'] = camera[float(metadata['RESTWAV'])*1E06]
            metadata['WAVELNTH'] = metadata['RESTWAV']
        except:
            metadata['CAMERA'] = "N/A"
            metadata['WAVELNTH'] = "N/A"
    
    metadata["VO"] = False
    
    return metadata

def get_all_metadata_pacs_spectro(fitsfile):
    """ Get all metadata need for ingestion in database"""
    
    warnings.filterwarnings('ignore')
    fits = pyfits.open(fitsfile,mode='update')
    # UPDATE Cube Header for Final Cubes
    try:
        waveArray = fits["ImageIndex"].data.field(0)
        cdelt3 = (waveArray[-1]-waveArray[0])/len(waveArray)
        crpix3 = int(len(waveArray)/2.)
        crval3 = waveArray[0] + crpix3 * cdelt3
        fits[1].header.set(keyword='CDELT3',value=cdelt3,comment="[] WCS: Pixel scale axis 3",after='CDELT2')
        fits[1].header.set(keyword='CRPIX3',value=crpix3,comment="[] WCS: Reference pixel position axis 3, unit=Scalar",after='CRPIX2')
        fits[1].header.set(keyword='CRVAL3',value=crval3,comment="[] WCS: Third coordinate of reference pixel",after='CRVAL2')
    except:
        pass
    fits.close()

    metadata = tools.get_all_metadata(fitsfile)
    
    filename = metadata["FILENAME"].lower()
    
    if filename.count("frame")>0:                                metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Frame", "Level1"
    if filename.count("rastercube")>0:                           metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "RasterCube", "Level1"
    if filename.count("projectedcube")>0:                        metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "SpecProject FinalCube", "Level2"
    if filename.count("mapflux")>0:                              metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "MapFlux", "Level25"
    if filename.count("drizzledcube")>0:                         metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Drizzle FinalCube", "Level25"
    if filename.count("regularspatialgridinterpolationcube")>0:  metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Regular Spatial Grid Interpolation FinalCube", "Level25"
    
    metadata["PROCESSING"] = "HIPE Only"
    if filename.count("DegIAS")>0: metadata["PROCESSING"] = "HIPE + Removed Dippers IAS"
    if filename.count("DegPACSman")>0: metadata["PROCESSING"] = "HIPE + Removed Dippers PACSman"
    
    if filename.count("mapflux")==1 : 
        metadata["BUILDVERSION"] = "See Corresponding FinalCube"
        metadata["NAXIS3"] = "0"
    
    try: metadata["ALGORITHM"] = metadata["ALGORITH"]
    except: pass
    
    try: metadata["RASTERID"] = metadata["RASID"]
    except: pass
    
    metadata['XYZ_X'], metadata['XYZ_Y'], metadata['XYZ_Z'] = tools.ra_dec_to_xyz(float(metadata['RA']),float(metadata['DEC']))
    
    metadata["VO"] = False
    
    return metadata

def get_all_metadata_pacs_spectro_l25(fitsfile):
    """ Get all metadata need for ingestion in database"""
    metadata = {}
    
    filepathComplete = str(fitsfile)
    fits = pyfits.open(filepathComplete, mode='update')
    filename = os.path.basename(filepathComplete)
    filepath = "/"+release.capitalize()+filepathComplete.split(release.capitalize())[1]
    
    metadata["FILENAME"] = filename
    metadata["FILEPATH"] = tools.correct_symbols_in_filepath(filepath)
    metadata["IMAGEPATH"] = metadata["FILEPATH"][:len(filepath)-5]+".png".replace("_raw","").replace("_wm-2px-1","")
    metadata["FILESIZE"] = tools.convert_megabytes(os.path.getsize(filepathComplete))
    metadata["PROGRAM"] = filepathComplete.split("/")[-3]
    metadata["CHAMP"]   = filepathComplete.split("/")[-2]
    metadata["PRODUCT"] = "MapFlux"
    metadata["LINE"] = filepathComplete.split("/")[-2]
    metadata["PROCESSING"] = "HIPE RasterCube + PACSman"

    #HDR0
    hdr0 = fits[0].header
    metadata["PUBLIC"] = tools.is_public(hdr0['DATE-OBS'])
    metadata.update(get_metadata_from_header(hdr0))

    if filename.count("Flux")>0:
        metadata["DESCRIP"] = "Spaxel line flux projection onto an oversampled grid."
        if filename.count("Flux.fits")>0: metadata["DESCRIP"] = metadata["DESCRIP"] + " Filled holes."
    if filename.count("spectra")>0:
        metadata["DESCRIP"] = "Spaxel spectra projection onto an oversampled grid."
    metadata["DESCRIP"] = metadata["DESCRIP"] + " " + metadata["BUNIT"]
    
    metadata["DURATION"] = hdr0['EQUINOX']
    
    metadata['XYZ_X'], metadata['XYZ_Y'], metadata['XYZ_Z'] = tools.ra_dec_to_xyz(float(metadata['RA']),float(metadata['DEC']))

    #Rajout MetaData
    if filename.count("Flux")>0:
        mapDescrip = {"Map1":"Flux Map", "Map2":"Noise", "Map3":"Velocity Map", "Map4":"Velocity Map Error", "Map5":"FWHM Map", "Map6":"FWHM Map Error", "Map7":"Continuum Map", "Map8":"Continuum Map"}
        for key,value in mapDescrip.iteritems(): hdr0.update(key,value)
    
    basePathCorres = filepathComplete.replace("PACSman_Fits","HIPE_Fits").split(metadata["LINE"])[0]
    #print basePathCorres
    motifs = ["ProjectedCube.fits",metadata['OBS_ID']]
    correspondingProjectedCube = tools.get_files_pattern(basePathCorres,motifs,[])
    if len(correspondingProjectedCube)==1:
        #print correspondingProjectedCube[0]
        fits_Corres = pyfits.open(correspondingProjectedCube[0])
        dateObs = fits_Corres[0].header["DATE-OBS"]
        fits_Corres.close()
        metadata["PUBLIC"] = is_public(dateObs)
        hdr0.update("DATE-OBS",str(dateObs))
    else: metadata["PUBLIC"] = False
    
    fits.close()
    
    metadata["PRODUCT_LEVEL"] = "Level25"
    
    return metadata


def table_pacs(table,conn=None,col=False):
    
    if table == "pacs_photo_l1":
        sql = "CREATE TABLE pacs_photo_l1 \
            (\
            l1_id serial primary key,\
            obsid integer[],\
            object varchar,\
            observer varchar,\
            cusmode varchar,\
            odnumber integer,\
            creationdate varchar,\
            startdate varchar,\
            enddate varchar,\
            obsmode varchar,\
            pointingmode varchar,\
            aorlabel varchar,\
            equinox varchar,\
            radesys varchar,\
            ranominal real,\
            decnominal real,\
            ra real,\
            dec real,\
            blue varchar,\
            repfactor integer,\
            camera varchar,\
            filepath varchar,\
            program varchar,\
            processing varchar,\
            filename varchar,\
            x real,\
            y real,\
            z real,\
            filesize_bytes real,\
            filesize real,\
            public varchar,\
            release varchar,\
            last varchar,\
            metadata json\
            )"
    
    if table == "pacs_photo_l2":
        sql = "CREATE TABLE pacs_photo_l2 \
            (\
            l2_id serial primary key,\
            object varchar,\
            filepath varchar,\
            observer varchar,\
            public varchar,\
            program varchar,\
            processing varchar,\
            filename varchar,\
            image varchar,\
            fullsize_image varchar,\
            crval1 real,\
            crval2 real,\
            cdelt1 real,\
            cdelt2 real,\
            crota2 real,\
            naxis1 integer,\
            naxis2 integer,\
            crpix1 real,\
            crpix2 real,\
            ctype1 varchar,\
            ctype2 varchar,\
            equinox varchar,\
            wavelength real,\
            description varchar,\
            buildversion varchar,\
            x real,\
            y real,\
            z real,\
            filesize_bytes real,\
            filesize real,\
            release varchar,\
            last varchar,\
            spoly spoly,\
            metadata json\
            )"
     
    if table == "pacs_spectro_l1":
        sql = "CREATE TABLE pacs_spectro_l1 \
            (\
            l1_id serial primary key,\
            object varchar,\
            obsid integer[],\
            observer varchar,\
            camera varchar,\
            ra real,\
            dec real,\
            x real,\
            y real,\
            z real,\
            observingmode varchar,\
            algorithm varchar,\
            line varchar,\
            rasterid varchar,\
            isoffposition varchar,\
            naxis1 integer,\
            naxis2 integer,\
            naxis3 integer,\
            program varchar,\
            product varchar,\
            processing varchar,\
            filename varchar,\
            filepath varchar,\
            filesize real,\
            public varchar,\
            release varchar,\
            last varchar,\
            metadata json\
            )"
     
    if table == "pacs_spectro_l2":
        sql = "CREATE TABLE pacs_spectro_l2 \
            (\
            l2_id serial primary key,\
            object varchar,\
            obsid integer[],\
            observer varchar,\
            camera varchar,\
            ra real,\
            dec real,\
            x real,\
            y real,\
            z real,\
            observingmode varchar,\
            algorithm varchar,\
            line varchar,\
            builversion varchar,\
            naxis1 integer,\
            naxis2 integer,\
            naxis3 integer,\
            program varchar,\
            product varchar,\
            processing varchar,\
            filename varchar,\
            image varchar,\
            filepath varchar,\
            filesize real,\
            public varchar,\
            release varchar,\
            last varchar,\
            metadata json\
            )"
     
    if table == "pacs_spectro_l25":
        sql = "CREATE TABLE pacs_spectro_l25 \
            (\
            l25_id serial primary key,\
            object varchar,\
            obsid integer[],\
            ra real,\
            dec real,\
            x real,\
            y real,\
            z real,\
            unit varchar,\
            line varchar,\
            builversion_hipe varchar,\
            version_pacsman varchar,\
            naxis1 integer,\
            naxis2 integer,\
            program varchar,\
            product varchar,\
            descrip varchar,\
            processing varchar,\
            filename varchar,\
            image varchar,\
            filepath varchar,\
            filesize real,\
            public varchar,\
            release varchar,\
            last varchar,\
            metadata json\
            )"
    
    if conn!=None:
        print "create table "+table
        cur = conn.cursor()
        cur.execute(sql)
        cur.close()
    else:
        columns = sql.split('(', 1)[1].split(')')[0]
        if col==False: 
            keys = [ i.lstrip().rstrip().split(" ")[0].upper() for i in columns.split(",") ]
            return keys[1:] #the first column is auto incremented
        else:
            return [ i.lstrip().rstrip() for i in columns.split(",")]
