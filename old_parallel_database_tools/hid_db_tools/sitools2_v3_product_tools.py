import fill_table
import json
import requests, simplejson

def get_sqlColumnType(type):
    key_type = {}
    key_type.update({"serial":   {"sqlColumnType":"serial", "javaSqlColumnType":4}})
    key_type.update({"integer":  {"sqlColumnType":"int4",   "javaSqlColumnType":4}})
    key_type.update({"integer[]":{"sqlColumnType":"_int4",  "javaSqlColumnType":2003}})
    key_type.update({"varchar":  {"sqlColumnType":"varchar","javaSqlColumnType":12}})
    key_type.update({"real":     {"sqlColumnType":"float4", "javaSqlColumnType":7}})
    key_type.update({"json":     {"sqlColumnType":"json",   "javaSqlColumnType":1111}})
    key_type.update({"spoly":    {"sqlColumnType":"spoly",  "javaSqlColumnType":1111}})
    
    return key_type[type]

def get_table_keys_type(table):
    columns = fill_table.keys_table(table,conn=None,col=True)
    return columns

def get_columnModel(table):
    
    columns = get_table_keys_type(table)
    all_columnModel = []
    for column in columns:
        column_name = column.split(" ")[0]
        column_type = column.split(" ")[1]
        columnModel =  {
         "dataIndex":column_name,
         "header":column_name,
         "width":100,
         "sortable":True,
         "visible":True,
         "filter":True,
         "sqlColumnType":get_sqlColumnType(column_type)["sqlColumnType"],
         "schema":"public",
         "tableName":table,
         "specificColumnType":"DATABASE",
         "columnAlias":column_name,
         "javaSqlColumnType":get_sqlColumnType(column_type)["javaSqlColumnType"]
        }
        if "primary key" in column:
            columnModel.update(
            {
            "orderBy":"ASC",
            "primaryKey":True,
            "visible":False
            })
        
        all_columnModel.append(columnModel)
    
    download_column =  {
         "dataIndex":"'http://idoc-herschel-test.ias.u-psud.fr/sitools/datastorage/user/storageRelease'||"+'"'+table+'".filepath',
         "header":"download",
         "width":100,
         "sortable":True,
         "visible":True,
         "filter":False,
         "sqlColumnType":"String",
         "specificColumnType":"SQL",
         "columnAlias":"download",
         "javaSqlColumnType":0,
         "columnRenderer": {"behavior": "localUrl", "linkText": "Download"}
        }
    all_columnModel.append(download_column)
    
    thumbnail_column =  {
         "dataIndex":"'http://idoc-herschel-test.ias.u-psud.fr/sitools/datastorage/user/storageRelease'||"+'"'+table+'".image',
         "header":"thumbnail",
         "width":100,
         "sortable":True,
         "visible":False,
         "filter":False,
         "sqlColumnType":"String",
         "specificColumnType":"SQL",
         "columnAlias":"thumbnail",
         "javaSqlColumnType":0,
        }
    all_columnModel.append(thumbnail_column)
    
    if table.count("photo_l2")>0:
        preview_column =  {
         "dataIndex":"'http://idoc-herschel-test.ias.u-psud.fr/sitools/datastorage/user/storageRelease'||"+'"'+table+'".fullsize_image',
         "header":"preview",
         "width":100,
         "sortable":True,
         "visible":True,
         "filter":False,
         "sqlColumnType":"String",
         "specificColumnType":"SQL",
         "columnAlias":"preview",
         "javaSqlColumnType":0,
         "columnRenderer": {"behavior": "ImgThumbSQL", "columnAlias": "thumbnail", "linkText": "Preview"}
        }
        all_columnModel.append(preview_column)
    
    #if table.count("l1")==1: id = "l1_id"
    #if table.count("l2")==1: id = "l2_id"
    #if table.count("l25")==1: id = "l25_id"
    #header_column =  {
        #"columnAlias": "header",
        #"columnRenderer": {"behavior": "localUrl", "linkText": "Show"},
        #"dataIndex": "'http://idoc-herschel-test.ias.u-psud.fr/ds/priv/lensmalhotraspirephotol2prevrelease/plugin/getHeaderFits?1=1&p[0]=LISTBOXMULTIPLE|"+id+"|'||"+'"'+table+'".'+id,
        #"header": "header",
        #"javaSqlColumnType": 0,
        #"schema": "public",
        #"specificColumnType": "SQL",
        #"sqlColumnType": "String",
        #"visible": True,
        #"width": 55
    #}
    #all_columnModel.append(header_column)
    
    return all_columnModel

def set_sql_clause(table,column,comparator,value):
    
    column_name = column.split(" ")[0]
    column_type = column.split(" ")[1]
    sql_clause = [
            {
             "closedParenthesis":"",
             "openParenthesis":"",
             "logicOperator":"and",
             "compareOperator":comparator,
             "leftAttribute":{  
                "tableName":table,
                "dataIndex":column_name,
                "schema":"public",
                "columnAlias":column_name,
                "specificColumnType":"DATABASE",
                "sqlColumnType":get_sqlColumnType(column_type)["sqlColumnType"],
                "javaSqlColumnType":get_sqlColumnType(column_type)["javaSqlColumnType"]
             },
             "rightValue":value
            }
          ]
      
    return sql_clause

def create_dataset_post(table,dataset_name,dataset_description,sql_clause):
    post = {
       "id":"",
       "name":dataset_name,
       "description":dataset_description,
       "image":{  
          "url":"",
          "type":"Image",
          "mediaType":"Image"
       },
       "datasource":{  
          "id":"2963d6f3-af5f-430e-9c38-84e2334bae1f",
          "type":"datasource",
          "mediaType":"datasource"
       },
       "sitoolsAttachementForUsers":"",
       "descriptionHTML":"",
       "dirty":"false",
       "datasetView":{  
          "id":"5854b9b9-58fc-489f-ba66-48fcdffde657",
          "name":"Sitools Infinite Scrolling View",
          "description":"Sitools LiveGrid",
          "jsObject":"sitools.user.component.datasets.dataviews.Livegrid",
          "fileUrl":"",
          "priority":0
       },
       "datasetViewConfig":[  
          {  
             "name":"lineHeight",
             "value":25
          },
          {  
             "name":"autoFitColumn",
             "value":False
          }
       ],
       "properties":[  

       ],
       "visible":False,
       "queryType":"W",
       "distinct":False,
       "sqlQuery":"",
       "predicat":sql_clause,
       "structures":[  
          {  
             "name":table,
             "schemaName":"public",
             "type":"table"
          }
       ],
       "columnModel":get_columnModel(table),
       "structure":{  
          "mainTable":{  
             "name":table,
             "schema":"public"
          },
          "nodeList":[]
       }
    }

    return post

def get_dataset_info(dataset_properties_list):
    
    all_dataset_info = []
    
    for dataset_properties in dataset_properties_list:
        all_dataset_info.append({  
            "id":dataset_properties["id"],
            "description":dataset_properties["description"],
            "name":dataset_properties["name"],
            "mediaType":"DataSet",
            "type":"DataSet",
            "visible":False,
            "status":"ACTIVE",
            "properties":[  
                {  
                    "name":"nbRecord",
                    "value":str(dataset_properties["nbRecords"])
                },
                {  
                    "name":"descriptionHTML",
                    "value":""
                },
                {  
                    "name":"imageUrl",
                    "value":""
                }
            ],
            "url":"/"+dataset_properties["name"]
        })
    
    return all_dataset_info

def create_project_post(project_name,dataset_properties_list):
    
    post = {  
        "id":"",
        "maintenance":"",
        "categoryProject":"",
        "priority":"",
        "name":project_name,
        "description":"All "+project_name+" datasets",
        "sitoolsAttachementForUsers":"",
        "image":{  
            "url":"",
            "type":"Image",
            "mediaType":"Image"
        },
        "htmlDescription":"",
        "maintenanceText":"",
        "visible":False,
        "navigationMode":"fixed",
        "css":"",
        "ftlTemplateFile":"hesiod_default.project.ftl",
        "htmlHeader":"",
        "dataSets":get_dataset_info(dataset_properties_list),
        "modules":[  
            {  
                "description":"Project Graph",
                "id":"projectGraph",
                "name":"projectGraph",
                "priority":0,
                "categoryModule":"",
                "divIdToDisplay":"graph",
                "xtype":"sitools.user.modules.ProjectGraphModule",
                "listProjectModulesConfig":[  
                    {  
                        "name":"columns",
                        "value":"[{\"columnName\":\"records\",\"selected\":true,\"width\":\"\",\"color\":\"\"},{\"columnName\":\"image\",\"selected\":false,\"width\":\"\",\"color\":\"\"},{\"columnName\":\"description\",\"selected\":false,\"width\":\"\",\"color\":\"\"},{\"columnName\":\"data\",\"selected\":true,\"width\":\"\",\"color\":\"\"},{\"columnName\":\"definition\",\"selected\":false,\"width\":\"\",\"color\":\"\"},{\"columnName\":\"feeds\",\"selected\":false,\"width\":\"\",\"color\":\"\"},{\"columnName\":\"opensearch\",\"selected\":false,\"width\":\"\",\"color\":\"\"}]"
                    },
                    {  
                        "name":"additionalColumns",
                        "value":"[]"
                    }
                ],
                "label":"label.projectGraph"
            },
            {  
                "description":"Feeds for Projects",
                "id":"ProjectsFeeds",
                "name":"ProjectsFeeds",
                "priority":1,
                "categoryModule":"",
                "divIdToDisplay":"",
                "xtype":"sitools.user.modules.FeedsProjectModule",
                "label":"label.projectFeeds"
            },
            {  
                "description":"Forms Project",
                "id":"formsModule",
                "name":"Forms",
                "priority":2,
                "categoryModule":"",
                "divIdToDisplay":"forms",
                "xtype":"sitools.user.modules.FormModule",
                "label":"label.forms"
            },
            {  
                "description":"Show a Project Description",
                "id":"4570d4e4-e5d1-4e7b-8ff4-9c3b400320cf",
                "name":"projectDescription",
                "priority":3,
                "categoryModule":"",
                "divIdToDisplay":"",
                "xtype":"sitools.user.modules.ProjectDescription",
                "label":"label.projectDescription"
            }
        ],
        "links":[  
            {  
                "name":"label.legalInformations",
                "url":"/sitools/client-public/html/legalInformations.html"
            },
            {  
                "name":"label.personalInformations",
                "url":"/sitools/client-public/html/personalInformations.html"
            },
            {  
                "name":"label.contacts",
                "url":"/sitools/client-public/html/contacts.html"
            },
            {  
                "name":"label.help",
                "url":"/sitools/client-public/html/help.html"
            },
            {  
                "name":"label.editorialInformations",
                "url":"/sitools/client-public/html/editorialInformations.html"
            }
        ]
    }
    
    return post

def getListAllDs(urlDs,auth,headers):
    listAllDs = []
    rGetAllDs = requests.get(urlDs,headers=headers,auth=auth)
    if rGetAllDs.status_code == 200:
        dataDs = json.loads(rGetAllDs.content)
        for ds in dataDs["data"]:
            listAllDs.append(ds["id"])
    return listAllDs 

def delListDs(url,auth,headers,listDsToDel):
    for id in listDsToDel: 
        print id
        rDel = requests.delete(url+id, auth=auth)

def removeDefaultGUIServices(url,service_name,header,auth):
    url_gui_serv = url + "guiServices/" + service_name
    print url_gui_serv,
    request = requests.get(url=url_gui_serv,headers=header,auth=auth)
    post = json.loads(request.content)["guiService"]
    post["defaultGuiService"] = False
    post["defaultVisibility"] = False
    post = simplejson.dumps(post,indent=4)
    put = requests.put(url=url_gui_serv,headers=header,data=post,auth=auth)
    print put.status_code
    

def download_service_post():
    
    return {
        "classVersion": "0.9.3",
        "classAuthor": "Akka Technologies",
        "classOwner": "CNES",
        "description": "Order resources associated to metadata, creating a ZIP, TAR or TAR.GZ 'on the fly' and download it.",
        "className": "fr.cnes.sitools.resources.order.DirectOrderResourceModel",
        "resourceClassName": "fr.cnes.sitools.resources.order.OrderResourceFacade",
        "applicationClassName": "fr.cnes.sitools.dataset.DataSetApplication",
        "dataSetSelection": "MULTIPLE",
        "name": "DirectOrderResourceModel",
        "behavior": "DISPLAY_IN_DESKTOP",
        "parameters": [
        {
          "name": "url",
          "description": "attachment url",
          "type": "PARAMETER_ATTACHMENT",
          "value": "\/plugin\/download",
          "valueType": "xs:string",
          "userUpdatable": False
        },
        {
          "name": "methods",
          "description": "List of methods implemented for this resource, separate by |",
          "type": "PARAMETER_INTERN",
          "value": "GET",
          "valueType": "xs:enum-multiple[GET, POST, PUT, DELETE]",
          "userUpdatable": False
        },
        {
          "name": "image",
          "description": "The image url",
          "type": "PARAMETER_INTERN",
          "value": "",
          "valueType": "xs:image",
          "userUpdatable": False
        },
        {
          "name": "runTypeAdministration",
          "description": "The intern run type",
          "type": "PARAMETER_INTERN",
          "value": "TASK_FORCE_RUN_SYNC",
          "valueType": "xs:enum[TASK_FORCE_RUN_SYNC,TASK_FORCE_RUN_ASYNC,TASK_DEFAULT_RUN_SYNC,TASK_DEFAULT_RUN_ASYNC]",
          "userUpdatable": False
        },
        {
          "name": "resourceImplClassName",
          "description": "The name of the resource implementation",
          "type": "PARAMETER_INTERN",
          "value": "fr.cnes.sitools.resources.order.DirectOrderResource",
          "valueType": "xs:string",
          "userUpdatable": False
        },
        {
          "name": "colUrl",
          "description": "Colum containing data url for order",
          "type": "PARAMETER_INTERN",
          "value": "download",
          "valueType": "xs:dataset.columnAlias",
          "userUpdatable": False
        },
        {
          "name": "too_many_selected_threshold",
          "description": "Maximum number of files allowed to be downloaded (-1 to set no limit)",
          "type": "PARAMETER_INTERN",
          "value": "-1",
          "valueType": "xs:integer",
          "userUpdatable": False
        },
        {
          "name": "max_warning_threshold",
          "description": "Maximum number of files allowed to be downloaded before client warning, download is still allowed",
          "type": "PARAMETER_USER_GUI",
          "value": "",
          "valueType": "xs:integer",
          "userUpdatable": False
        },
        {
          "name": "max_warning_threshold_text",
          "description": "Text to display to the user when Warning threshold is reached",
          "type": "PARAMETER_USER_GUI",
          "value": "",
          "valueType": "xs:string",
          "userUpdatable": False
        },
        {
          "name": "too_many_selected_threshold_text",
          "description": "Text to display to the user when TooMaxySelected threshold is reached",
          "type": "PARAMETER_USER_GUI",
          "value": "",
          "valueType": "xs:string",
          "userUpdatable": False
        },
        {
          "name": "fileName",
          "description": "The name of the file to generate. Fill it to download a file. Leave it Empty to view resource Representation.",
          "type": "PARAMETER_USER_INPUT",
          "value": "dataset_order_${date:yyyy-MM-dd HH_mm_ss}",
          "valueType": "xs:template",
          "userUpdatable": True
        },
        {
          "name": "runTypeUserInput",
          "description": "The user input run type",
          "type": "PARAMETER_USER_INPUT",
          "value": "",
          "valueType": "xs:enum[,TASK_RUN_SYNC,TASK_RUN_ASYNC]",
          "userUpdatable": False
        },
        {
          "name": "archiveType",
          "description": "The type of archive needed",
          "type": "PARAMETER_USER_INPUT",
          "value": "tar.gz",
          "valueType": "xs:enum[zip,tar.gz,tar]",
          "userUpdatable": True
        }
        ]
    }
    

def create_project_graph_post(project,dataset_properties_list):
    
    node = {}
    available_nodes = []
    for dataset_properties in dataset_properties_list:
        
        if dataset_properties["name"].count("spire_photo")>0:  nodeName= "Spire Photometer"
        if dataset_properties["name"].count("spire_fts")>0:    nodeName= "Spire FTS"
        if dataset_properties["name"].count("pacs_photo")>0:   nodeName= "Pacs Photometer"
        if dataset_properties["name"].count("pacs_spectro")>0: nodeName= "Pacs Spectrometer"
        if dataset_properties["name"].count("previous")>0:     nodeName= "Previous Releases"
        
        node[nodeName] = { "text": nodeName,"image": {},"description": "","children": [],"type": "node","leaf": False }
    
    for dataset_properties in dataset_properties_list:
        
        dataset_name = dataset_properties["name"]
        text = dataset_name.split(project+"_")[-1]
        if text.count("previous")==0:
            if text.count("l1")==1:  text = "Level 1"
            if text.count("l2")==1:  text = "Level 2"
            if text.count("l25")==1: text = "Level 25"
        id = dataset_properties["id"]
        nbRecords = dataset_properties["nbRecords"]
        
        dataset_info = {
            "text": text,
            "datasetId": id,
            "visible": False,
            "status": "ACTIVE",
            "nbRecord": nbRecords,
            "imageDs": "",
            "readme": "",
            "url": "/"+dataset_name,
            "leaf": True,
            "type": "dataset"

        }
        
        if dataset_properties["name"].count("spire_photo")>0:  nodeName= "Spire Photometer"
        if dataset_properties["name"].count("spire_fts")>0:    nodeName= "Spire FTS"
        if dataset_properties["name"].count("pacs_photo")>0:   nodeName= "Pacs Photometer"
        if dataset_properties["name"].count("pacs_spectro")>0: nodeName= "Pacs Spectrometer"
        if dataset_properties["name"].count("previous")>0:     nodeName= "Previous Releases"
        
        node[nodeName]["children"].append(dataset_info)
    
    wanted_order = ["Pacs Photometer","Pacs Spectrometer","Spire Photometer","Spire FTS","Previous Releases"]
    project_graph_post = { "id":None, "nodeList": [ node[nodeName] for nodeName in wanted_order if nodeName in node.keys() ] }
    
    return project_graph_post



