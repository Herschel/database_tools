'''
Created on Sep 22, 2015

@author: bhasnoun

Herschel Datatabase (HID)\n
Insertion of metadata in PostgreSQL, MongoDB, Pandas database via Pyscopg2, PyMongo
'''

if __name__ == "__main__":

    import time

    from hid_db_tools.tools import delete_release, get_connection, close_connection, get_listfile_to_inject
    from hid_db_tools.spire import fill_spire_photo_catalog, create_spire_pacs_l25_view
    from hid_db_tools.fill_table import fill_table_parallel, create_table
    
    t0 = time.time()
    total_fitsfiles = 0
    ################################################################################################

    drop = True
    verbose = False
    mode = "postgres" # "panda" or "postgres" or "mongodb"
    
    debug = False

    ###################################### Releases to process ######################################

    release_spire_photo  = ["R0_spire_photo","R1_spire_photo","R2_spire_photo","R3_spire_photo","R4_spire_photo","R5_spire_photo","R6_spire_photo"]
    release_spire_fts    = ["R0_spire_fts","R1_spire_fts","R2_spire_fts","R3_spire_fts","R4_spire_fts","R5_spire_fts"]
    release_pacs_photo   = ["R0_pacs_photo","R1_pacs_photo","R2_pacs_photo"]
    release_pacs_spectro = ["R0_pacs_spectro","R1_pacs_spectro","R2_pacs_spectro","R3_pacs_spectro","R4_pacs_spectro","R5_pacs_spectro"]

    ###################################### Last Releases ######################################

    last_spire_photo  = "R6_spire_photo"
    last_spire_fts    = "R5_spire_fts"
    last_pacs_photo   = "R2_pacs_photo"
    last_pacs_spectro = "R5_pacs_spectro"

    ###################################### Instrument/level to process ###############################
    ###################################### Uncomment your choice #####################################

    tables =  []
    
    #tables += ["spire_photo_l1"]
    #tables += ["spire_photo_l2"]
    #tables += ["spire_fts_l1"]
    tables += ["spire_fts_l2"]
    
    #tables += ["pacs_photo_l1"]
    #tables += ["pacs_photo_l2"]
    #tables += ["pacs_spectro_l1"]
    #tables += ["pacs_spectro_l2"]
    
    #tables += ["spire_photo_density"]
    #tables += ["spire_catalog"]
    #tables += ["pacs_spectro_l25"]
    #tables += ["spirepacs_photo_l25"]

    ################################################################################################
    ################################################################################################
    
    t = time.time()
    dropped = []
    tables_to_update = list(set(["spire_photo_l1","spire_photo_l2","spire_fts_l1","spire_fts_l2","spire_photo_density","spire_catalog"]).intersection(tables))
    if len(tables_to_update)!=0:
        
        for release in release_spire_photo + release_spire_fts:
            print release.upper()
            basepath = '/data/glx-herschel/data1/herschel/Release/'+release.capitalize()

            for table in [ table for table in tables_to_update if table.count(release[3:])>0 ]:

                if drop and table not in dropped:
                    create_table(table,mode)
                    dropped.append(table)

                listfile = get_listfile_to_inject(table, basepath)
                if debug: listfile = listfile[:2]
                total_fitsfiles += len(listfile)
                if len(listfile)>0:

                    if not drop: delete_release(table,release,mode)

                    if table.count("spire_photo_l")>0 or table.count("spire_fts")>0:
                        if table.count("spire_photo_l")>0: last = last_spire_photo
                        if table.count("spire_fts_l")>0: last = last_spire_fts
                        fill_table_parallel(listfile,table,last,mode,verbose)

                    if table == "spire_catalog":
                        fill_spire_photo_catalog(listfile,table,mode)

    print "Total time elapsed for SPIRE: {:.2f}s".format(time.time()-t)

    ################################################################################################
    ################################################################################################
    
    t = time.time()
    dropped = []
    tables_to_update = list(set(["pacs_photo_l1","pacs_photo_l2","pacs_spectro_l1","pacs_spectro_l2","pacs_spectro_l25"]).intersection(tables))
    if len(tables_to_update)!=0:
        
        for release in release_pacs_photo + release_pacs_spectro:
            print release.upper()
            basepath = '/data/glx-herschel/data1/herschel/Release/'+release.capitalize()

            for table in [ table for table in tables_to_update if table.count(release[3:])>0 ]:

                if drop and table not in dropped:
                    create_table(table,mode)
                    dropped.append(table)

                listfile = get_listfile_to_inject(table, basepath)
                if debug: listfile = listfile[:2]
                total_fitsfiles += len(listfile)
                if len(listfile)>0:

                    if not drop: delete_release(table,release,mode)

                    if table.count("pacs_photo_l")>0: last = last_pacs_photo
                    if table.count("pacs_spectro_l")>0: last = last_pacs_spectro
                    fill_table_parallel(listfile,table,last,mode,verbose)

    print "Total time elapsed for PACS: {:.2f}s".format(time.time()-t)

    ################################################################################################
    ################################################################################################

    tables_to_update = list(set(["spirepacs_photo_l25"]).intersection(tables))
    if len(tables_to_update)!=0:
        print tables_to_update
        create_spire_pacs_l25_view(last_spire_photo,last_pacs_photo,mode)

    ################################################################################################
    ################################################################################################
    
    print "TOTAL TIME: {:.2f}s".format(time.time()-t0)
    print "TOTAL FITS FILES INSERTED:", total_fitsfiles

