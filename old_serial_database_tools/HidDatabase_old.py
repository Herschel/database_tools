#!/usr/bin/python2.7
#
# Small script to show PostgreSQL and Pyscopg together
# insert metadata from PACS photo SPIRE photo and FTS  into the Herchel Datatabase (HID)
#
import psycopg2,glob
import os, string, shutil

old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")
execfile(old_serial_database_tools+"SPIRE_Photo_Tools.py")
execfile(old_serial_database_tools+"SPIRE_FTS_Tools.py")
execfile(old_serial_database_tools+"SPIRE_Catalog_Tools.py")
execfile(old_serial_database_tools+"PACS_Photo_Tools.py")
execfile(old_serial_database_tools+"PACS_Spectro_Tools.py")
execfile(old_serial_database_tools+"SANEPIC_Tools.py")


baseName = "'HID_glx'"
drop = False

#prefix_list_spire_photo=["attic","R0_spire_photo","R1_spire_photo","R2_spire_photo","R3_spire_photo","R4_spire_photo"]
#prefix_list_spire_photo = ["R4_spire_photo"]
prefix_list_spire_fts = ["R5_spire_fts"]
#prefix_list_pacs_photo = ["R2_pacs_photo","R1_pacs_photo","R0_pacs_photo","attic"]

last_dico_spire_photo = {"R5_spire_photo":True,"R4_spire_photo":False,"R3_spire_photo":False,"R2_spire_photo":False,"R1_spire_photo":False, "R0_spire_photo":False,"attic":False}
last_dico_spire_FTS = {"R5_spire_fts":True,"R4_spire_fts":False,"R3_spire_fts":False,"R2_spire_fts":False,"R1_spire_fts":False,"attic":False}
last_dico_pacs_photo = {"R2_pacs_photo":True,"R1_pacs_photo":False,"R0_pacs_photo":False,"attic":False}
last_dico_pacs_spectro = {"attic":False,"R0_pacs_spectro":False,"R1_pacs_spectro":False,"R2_pacs_spectro":False,"R3_pacs_spectro":False,"R4_pacs_spectro":False,"R5_pacs_spectro":True}

#Instrument to process
traitement = []
#traitement += ["SPIRE_PHOTO_L1","SPIRE_PHOTO_L2"] #SPIRE_PHOTO
#traitement += ["SPIRE_PHOTO_L2"]
traitement += ["SPIRE_FTS_L2"] #SPIRE_FTS
#traitement += ["PACS_PHOTO_L1","PACS_PHOTO_L2"] #PACS_PHOTO
#traitement += ["PACS_SPECTRO_L1","PACS_SPECTRO_L2","PACS_SPECTRO_L25"] #PACS_SPECTRO
#traitement += ["SPIRE_PHOTO_DENSITY"]
#traitement += ["SPIREPACS_PHOTO_L25"]
#traitement += ["SPIRE_CATALOG"]
#traitement += ["PACS_PHOTO_L2"]

##### connect to the database
try:
    conn = psycopg2.connect("dbname="+baseName+" user='sitools'")
    cur = conn.cursor()
except:
    print "I am unable to connect to the database"

if "SPIRE_PHOTO_L1" in traitement :
    
    listTable=["spire_photo_l1"]
    if drop:
        cur=drop_tables(listTable,cur)
        command='''create table SPIRE_Photo_L1 (L1_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,proposal varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar, elecSide varchar,biasMode varchar,timeOffset  real,timeDrift  real,invalidOffsetFlag varchar,adcErrFlag varchar,plwBiasAmpl real,pmwBiasAmpl real, pswBiasAmpl real,ptcBiasAmpl  real,rcRollApp varchar,obsMode varchar,aorLabel varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,
posAngle varchar,BuildVersion varchar,CalibrationVersion varchar,scriptID varchar,fluxConversionDone varchar,temperatureDriftCorrectionDone varchar,electricalCrosstalkCorrectionDone varchar,opticalCrosstalkCorrectionDone varchar,filepath varchar,program varchar,filename varchar,X real,Y real,Z real,filesize_bytes real,filesize real,public varchar,release varchar,processing varchar,last varchar)'''
        cur.execute(command)

    for prefix in prefix_list_spire_photo:
        # Test Release existence and delete lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        conn.commit()
        # SPIRE PHOTO L1 Files (Sanepic)
        basePath_L1_SPIRE_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/L1_Sanepic_SPIRE/'
        listfile_L1_all=getListFiles(basePath_L1_SPIRE_Photo)
        print basePath_L1_SPIRE_Photo
        FillSPIREPhotoL1Table(listfile_L1_all,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()
    
if "SPIRE_PHOTO_L2" in traitement :
    
    listTable = ["spire_photo_l2","spire_photo_l2_obsids"]
    if drop:
        cur=drop_tables(listTable,cur)
        #create table for SPIRE Photo L2
        cur.execute('''create table SPIRE_Photo_L2 (L2_id integer PRIMARY KEY,object varchar,filepath varchar,observer varchar,proposal varchar,public varchar,program varchar,processing varchar,filename varchar,image varchar,fullsize_image varchar,crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,equinox varchar,wavelength real,description varchar,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,combine varchar,last varchar,spoly spoly,obsid integer,level25 varchar)''')
        cur.execute('''create table SPIRE_Photo_L2_obsids (L2_obsids_id integer,L2_id integer,obsid integer,L1_id integer,processing varchar,release varchar)''')
    
    for prefix in prefix_list_spire_photo:
        # Test Release existence and delete lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        conn.commit()
        # SPIRE PHOTO L2 HIPE 
        basePath_L2_SPIRE_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/MAPS_SPIRE/'
        listfile_L2_all=getListFiles(basePath_L2_SPIRE_Photo)
        motifs = ["fits"]
        exclure = ["color","density"]
        listfile_L2_all = getFilesMotif(basePath_L2_SPIRE_Photo,motifs,exclure)
        FillSPIREPhotoL2Table(listfile_L2_all,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()
        updateToLevel25(listTable,listfile_L2_all,prefix,cur)
        conn.commit()
        
        # SPIRE PHOTO L2 SANEPIC 
        basePath_L2_SPIRE_Photo_Sanepic = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/MAPS_SPIRE/'
        listfile_L2_SPIRE_Photo_Sanepic = getListFiles(basePath_L2_SPIRE_Photo_Sanepic)
        FillSanepicSPIREPhotoL2Table(listfile_L2_SPIRE_Photo_Sanepic,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()
        
        basePath_L2_SPIRE_Photo_Supreme = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SUPREME_Fits/MAPS_SPIRE/'
        motifs = ["SupremePhoto","fits"]
        exclure = ["CoAdd"]
        listfile_L2_SPIRE_Photo_Supreme = getFilesMotif(basePath_L2_SPIRE_Photo_Supreme,motifs,exclure)
        FillSPIREPhotoL2Table(listfile_L2_SPIRE_Photo_Supreme,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()

if "SPIRE_PHOTO_DENSITY" in traitement :

    listTable = ["spire_photo_density"]
    if drop:
        cur=drop_tables(listTable,cur)
        #create table for SPIRE Photo L2
        cur.execute('''create table SPIRE_Photo_Density (L2_id integer PRIMARY KEY,obsid integer,object varchar,filepath varchar,observer varchar,proposal varchar,program varchar,filename varchar,image varchar,crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,equinox varchar,description varchar,maxdens real,maxsig real,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,last varchar)''')

    for prefix in prefix_list_spire_photo:
        # Test Release existence and delete lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        conn.commit()
        # SPIRE PHOTO L2 HIPE 
        basePath_L2_SPIRE_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/MAPS_SPIRE/'
        listfile_L2_all=getListFiles(basePath_L2_SPIRE_Photo)
        motifs = ["colorsel"]
        exclure = []
        listfile_L2_all = getFilesMotif(basePath_L2_SPIRE_Photo,motifs,exclure)
        FillSPIREPhotoDensityTable(listfile_L2_all,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()


if "PACS_PHOTO_L1" in traitement :
    
    listTable = ["pacs_photo_l1"]
    if drop:
        cur=drop_tables(listTable,cur)
        #create table for PACS Photo L1
        cur.execute('''create table PACS_Photo_L1 (L1_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar,
        obsMode varchar,pointingMode varchar,aorLabel varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,blue varchar,repfactor integer,
        camera varchar,filepath varchar,program varchar,processing varchar,filename varchar,X real,Y real,Z real,filesize_bytes real,filesize real,public varchar,release varchar,last varchar)''')
    # PACS PHOTO L1 Files
    for prefix in prefix_list_pacs_photo:
        deleteRelease(listTable,cur,prefix.capitalize())
        conn.commit()
        basePath_L1_PACS_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/L1_PACS/'
        listfile_L1_PACS=getListFiles(basePath_L1_PACS_Photo)
        FillPACSPhotoL1Table(listfile_L1_PACS,cur,"red",prefix,last_dico_pacs_photo[prefix])
        FillPACSPhotoL1Table(listfile_L1_PACS,cur,"blue",prefix,last_dico_pacs_photo[prefix])
        FillPACSPhotoL1Table(listfile_L1_PACS,cur,"green",prefix,last_dico_pacs_photo[prefix])
        conn.commit()
        basePath_L1_PACS_Photo_Sanepic = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/L1_Sanepic_PACS/'
        listfile_L1_PACS_Sanepic=getListFiles(basePath_L1_PACS_Photo_Sanepic)
        FillSanepicPACSPhotoL1Table(listfile_L1_PACS_Sanepic,cur,"red",prefix,last_dico_pacs_photo[prefix])
        FillSanepicPACSPhotoL1Table(listfile_L1_PACS_Sanepic,cur,"blue",prefix,last_dico_pacs_photo[prefix])
        FillSanepicPACSPhotoL1Table(listfile_L1_PACS_Sanepic,cur,"green",prefix,last_dico_pacs_photo[prefix])
        conn.commit()

if "PACS_PHOTO_L2" in traitement :
    
    listTable = ["pacs_photo_l2","pacs_photo_l2_obsids"]
    if drop:
        cur = drop_tables(listTable,cur)
        #create table for PACS Photo L2
        cur.execute('''create table PACS_Photo_L2 (L2_id integer PRIMARY KEY,object varchar,filepath varchar,observer varchar,public varchar,program varchar,processing varchar,filename varchar,image varchar,fullsize_image varchar,crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,equinox varchar,wavelength real,description varchar,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,last varchar,spoly spoly)''')
        cur.execute('''create table PACS_Photo_L2_obsids (L2_obsids_id integer,L2_id integer,obsid integer,L1_id integer,processing varchar,release varchar)''')

    # PACS PHOTO L2 Files
    for prefix in prefix_list_pacs_photo:
        deleteRelease(listTable,cur,prefix.capitalize())
        conn.commit()
        basePath_L2_PACS_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()#+'/HIPE_Fits/MAPS_PACS/'
        motifs,exclure = ["MAPS_PACS",".fits"],["_MadMap_"]
        listfile_L2 = getFilesMotif(basePath_L2_PACS_Photo,motifs,exclure)
        #listfile_L2 = getListFiles(basePath_L2_PACS_Photo)
        FillPACSPhotoL2Table(listfile_L2,cur,prefix,last_dico_pacs_photo[prefix])
        conn.commit()
        
        # SPIRE PHOTO L2 SANEPIC L2 Files
        basePath_L2_PACS_Photo_Sanepic = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/MAPS_PACS/'
        listfile_L2_PACS_Photo_Sanepic = getListFiles(basePath_L2_PACS_Photo_Sanepic)
        FillSanepicPACSPhotoL2Table(listfile_L2_PACS_Photo_Sanepic,cur,"red",prefix,last_dico_pacs_photo[prefix])
        FillSanepicPACSPhotoL2Table(listfile_L2_PACS_Photo_Sanepic,cur,"blue",prefix,last_dico_pacs_photo[prefix])
        FillSanepicPACSPhotoL2Table(listfile_L2_PACS_Photo_Sanepic,cur,"green",prefix,last_dico_pacs_photo[prefix])
        conn.commit()

if "SPIRE_FTS_L1" in traitement :
    
    listTable = ["spire_fts_l1"]
    if drop:
        cur=drop_tables(listTable,cur)
        # create table for SPIRE FTS L1
        cur.execute('''create table SPIRE_FTS_L1 (L1_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar,timeOffset real,timeDrift real,biasFrequency real,obsMode varchar,aorLabel varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,posAngle varchar,numScans integer,commandedResolution varchar,mapSampling varchar,apodName varchar,BuildVersion varchar,CalibrationVersion varchar,filepath varchar,program varchar,origin varchar,filetype varchar,filename varchar,image varchar,X real,Y real,Z real,filesize real,public varchar,release varchar,last varchar)''')

    # SPIRE FTS L1 Files
    for prefix in prefix_list_spire_fts:
        # Test Release existence and delete matching lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        basePath_SPIRE_FTS = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/FTS_SPIRE/'
        listfile_FTS=getListFiles(basePath_SPIRE_FTS)
        FillSPIREFtsL1Table(listfile_FTS,cur,prefix,last_dico_spire_FTS[prefix])
        conn.commit()

if "SPIRE_FTS_L2" in traitement :
    
    listTable = ["spire_fts_l2"]
    if drop:
        cur=drop_tables(listTable,cur)
        # create table for SPIRE FTS L2
        cur.execute('''create table SPIRE_FTS_L2 (L2_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar,obsMode varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,posAngle varchar,commandedResolution varchar,mapSampling varchar,apodName varchar,projection varchar, wavelenth varchar, crval1 real,crval2 real,cdelt1 real,cdelt2 real,naxis integer,naxis1 integer,naxis2 integer,naxis3 integer,crpix1 integer,crpix2 integer,ctype1 varchar,ctype2 varchar,BuildVersion varchar,CalibrationVersion varchar,filepath varchar,program varchar,origin varchar,filename varchar,image varchar,X real,Y real,Z real,filesize real,public varchar,release varchar,last varchar )''')

    # SPIRE FTS L2 Files
    for prefix in prefix_list_spire_fts:
        # Test Release existence and delete matching lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        basePath_SPIRE_FTS = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/FTS_SPIRE/'
        motifs = ["_cube.fits"]
        exclure = ["extra"]
        listfile_FTS = getFilesMotif(basePath_SPIRE_FTS,motifs,exclure)
        FillSPIREFtsL2Table(listfile_FTS,cur,prefix,last_dico_spire_FTS[prefix])
        conn.commit()

if "PACS_SPECTRO_L1" in traitement :
    
    listTable = ["pacs_spectro_l1"]
    if drop:
        cur=drop_tables(listTable,cur)
        # create table for PACS Spectro L1
        cur.execute('''create table PACS_Spectro_L1 (L1_id integer PRIMARY KEY, object varchar, obsid varchar, observer varchar, camera varchar, ra real, dec real, x real, y real, z real, observingmode varchar, algorithm varchar, line varchar, rasterid varchar, isoffposition varchar, naxis1 integer, naxis2 integer, naxis3 integer, program varchar, product varchar, processing varchar, filename varchar, filepath varchar,filesize real,public varchar,release varchar,last varchar)''')

    # PACS SPECTRO L1 Files
    for prefix in prefix_list_pacs_spectro:
        # Test Release existence and delete matching lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        basePath_L1_SPECTRO = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/SPECTRO_PACS/'
        motifs = [".fits","L1"]
        exclure = ["_flag","Frame","_ProjectedCube","PACSman","MapFlux"]
        listfile_L1_SPECTRO = getFilesMotif(basePath_L1_SPECTRO,motifs,exclure)
        FillPACSspectroL1Table(listfile_L1_SPECTRO,cur,prefix,last_dico_pacs_spectro[prefix])
        conn.commit()

if "PACS_SPECTRO_L2" in traitement :
    
    listTable = ["pacs_spectro_l2"]
    if drop:
        cur=drop_tables(listTable,cur)
        cur.execute('''create table PACS_Spectro_L2 (L2_id integer PRIMARY KEY, object varchar, obsid varchar, observer varchar, camera varchar, ra real, dec real, x real, y real, z real, observingmode varchar, algorithm varchar, line varchar, builversion varchar, naxis1 integer, naxis2 integer, naxis3 integer, program varchar, product varchar, processing varchar, filename varchar, image varchar, filepath varchar,filesize real,public varchar,release varchar,last varchar)''')
    
    for prefix in prefix_list_pacs_spectro:
        # Test Release existence and delete matching lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        basePath_L2_SPECTRO = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/SPECTRO_PACS/'
        motifs = ["Cube.fits","L2"]
        exclure = ["MapFlux","PACSman","SAG-4/HD37041","RebinnedCube","RebinnedCubeAfterSub","RasterCube","RasterCubeBFF","Frame"]
        listfile_L2_SPECTRO = getFilesMotif(basePath_L2_SPECTRO,motifs,exclure)
        FillPACSspectroL2Table(listfile_L2_SPECTRO,cur,prefix,last_dico_pacs_spectro[prefix])
        conn.commit()

if "PACS_SPECTRO_L25" in traitement :
    
    listTable = ["pacs_spectro_l25"]
    if drop:
        cur=drop_tables(listTable,cur)
        cur.execute('''create table PACS_Spectro_L25 (L25_id integer PRIMARY KEY, object varchar, obsid varchar, ra real, dec real, x real, y real, z real, unit varchar, line varchar, builversion_HIPE varchar, version_PACSman varchar, naxis1 integer, naxis2 integer, program varchar, product varchar, descrip varchar, processing varchar, filename varchar, image varchar, filepath varchar,filesize real, public varchar, release varchar,last varchar)''')
    
    for prefix in prefix_list_pacs_spectro:
        # Test Release existence and delete matching lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        basePath_L25_SPECTRO = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/PACSman_Fits/SPECTRO_PACS/SAG-4/'
        motifs = [".fits"]
        exclure = ["RasterCubeCorrPACSman","mask","smooth","spectra"]
        listfile_L25_SPECTRO = getFilesMotif(basePath_L25_SPECTRO,motifs,exclure)
        FillPACSspectroL25Table(listfile_L25_SPECTRO,cur,prefix,last_dico_pacs_spectro[prefix])
        conn.commit()

if "SPIREPACS_PHOTO_L25" in traitement :
    releaseSPIRE="R5_spire_photo"
    releasePACS="R1_pacs_photo"
    CreateSPIREPACSL25View(cur,releaseSPIRE,releasePACS)
    conn.commit()

if "SPIRE_CATALOG" in traitement :
    
    listTable = ["spire_catalog"]
    if drop:
        cur=drop_tables(listTable,cur)
        cur.execute('''create table SPIRE_Catalog (source_id integer PRIMARY KEY, ra real,dec real,x real,y real,raPlusErr real,decPlusErr real,raMinusErr real,decMinusErr real,xPlusErr real,yPlusErr real,xMinusErr real,yMinusErr real,flux real,fluxPlusErr real,fluxMinusErr real,background real,bgPlusErr real,bgMinusErr real,quality real,object varchar,proposal varchar,band varchar,obsid integer)''')
    
    for prefix in prefix_list_spire_photo:
        # Test Release existence and delete matching lines if needed
        #deleteRelease(listTable,cur,prefix.capitalize())
        basePath_SPIRE_CATALOG = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/CATALOG_SPIRE/'
        motifs = ["catalog","mask","beam",".fits"]
        exclure = []
        listfile_SPIRE_CATALOG = getFilesMotif(basePath_SPIRE_CATALOG,motifs,exclure)
        FillSPIRECatalogTable(listfile_SPIRE_CATALOG,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()

cur.close()
conn.close()
