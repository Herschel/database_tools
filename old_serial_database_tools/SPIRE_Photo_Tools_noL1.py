import psycopg2
import pyfits
import glob
import os, string, shutil
import time
from datetime import date
import math

old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")

def initDico():
        DICO={}
        DICO["'BuildVersion'"]='BuildVersion'
        DICO["'CalibrationVersion'"]='CalibrationVersion'
        DICO["'temperatureDriftCorrectionDone'"]='temperatureDriftCorrectionDone'
        DICO["'timeOffset'"]='timeOffset'
        DICO["'timeDrift'"]='timeDrift'
        DICO["'invalidOffsetFlag'"]='invalidOffsetFlag'
        DICO["'adcErrFlag'"]='adcErrFlag'
        DICO["'ptcBiasAmpl'"]='ptcBiasAmpl'
        DICO["'rcRollApp'"]='rcRollApp'
        DICO["'electricalCrosstalkCorrectionDone'"]='electricalCrosstalkCorrectionDone'
        DICO["'opticalCrosstalkCorrectionDone'"]='opticalCrosstalkCorrectionDone'
        DICO["'fluxConversionDone'"]='fluxConversionDone'
        DICO["'scriptID'"]='scriptID'
        DICO['OBS_ID']='OBS_ID'
        DICO['OBJECT']='OBJECT'
        DICO['OBSERVER']='OBSERVER'
        DICO['CUSMODE']='CUSMODE'
        DICO['ODNUMBER']='ODNUMBER'
        DICO['ELECSIDE']='ELECSIDE'
        DICO['BIASMODE']='BIASMODE'
        DICO['PLWBIASA']='PLWBIASA'
        DICO['PMWBIASA']='PMWBIASA'
        DICO['PSWBIASA']='PSWBIASA'
        DICO['ELECSIDE']='ELECSIDE'
        DICO['RA']='RA'
        DICO['DEC']='DEC'
        DICO['OBS_MODE']='OBS_MODE'
        DICO['AOR']='AOR'
        DICO['EQUINOX']='EQUINOX'
        DICO['DESC']='DESC'
        DICO['RADESYS']='RADESYS'
        DICO['RA_NOM']='RA_NOM'
        DICO['DEC_NOM']='DEC_NOM'
        DICO['POSANGLE']='POSANGLE'
        DICO['PROPOSAL']='PROPOSAL'
        return DICO

def initDico2():
        DICO={}
        DICO['BuildVersion']=None
        DICO['CalibrationVersion']=None
        DICO['temperatureDriftCorrectionDone']=None
        DICO['timeOffset']=None
        DICO['timeDrift']=None
        DICO['invalidOffsetFlag']=None
        DICO['adcErrFlag']=None
        DICO['ptcBiasAmpl']=None
        DICO['rcRollApp']=None
        DICO['electricalCrosstalkCorrectionDone']=None
        DICO['opticalCrosstalkCorrectionDone']=None
        DICO['fluxConversionDone']=None
        DICO['scriptID']=None
        DICO['OBJECT']=None
        DICO['OBSERVER']=None
        DICO['CUSMODE']=None
        DICO['ODNUMBER']=None
        DICO['ELECSIDE']=None
        DICO['BIASMODE']=None
        DICO['PLWBIASA']=None
        DICO['PMWBIASA']=None
        DICO['PSWBIASA']=None
        DICO['RA']=None
        DICO['DEC']=None
        DICO['OBS_MODE']=None
        DICO['AOR']=None
        DICO['EQUINOX']=None
        DICO['DESC']=None
        DICO['RADESYS']=None
        DICO['RA_NOM']=None
        DICO['DEC_NOM']=None
        DICO['POSANGLE']=None
        DICO['PROPOSAL']=None
        return DICO


	
def FillSPIREPhotoL1Table(listfile,cur,prefix,last):
	XYZ=[0.,0.,0.]
	#listR1=["OT2_hdole","DDT_mustdo","SAG-4","OT1_lmontier"]
	last_orig=last
	L1_id=0
	for i in listfile:
		metadata=[]
		DICO=initDico()
		DICO2=initDico2()
		if  str(i).count("_dbl")==0 and  str(i).count("SMPVT")==0 and str(i).count("fixed")==0 and str(i).count("test")==0 and str(i).count("_all")==1 and str(i).count("_ExtEmiGainsApplied")==0 :
			filepath=str(i)
			print filepath
			processing="HIPE_Sanepic"
			program = filepath.split("/")[-3]
			#if program in listR1 and prefix=="R0_spire_photo":
		#		last=False
		#	else:
		#		last=last_orig
			filename=os.path.basename(filepath)
			dirpath=os.path.dirname(filepath)
			hdulist=pyfits.open(filepath)
			hdr0=hdulist[0].header
			Public=isPublic(hdr0['DATE-OBS'])
			#filepath=filepath.replace("/data/glx-herschel/data1/herschel/","/archive/HERSCHEL/")
			# create dictionnary to make correspondance between META and real name
			cards = hdr0.cards
			for card in cards:
				chaine_card=str(card)
				if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==1:
					#print chaine_card
					chaine_split=chaine_card.split("=")
					#print "Chaine 1",chaine_split[1].strip()
					chaine_resplit=chaine_split[0].split()
					chaine_resplit=chaine_resplit[1].split(".")
					#print "Chaine resplit",chaine_resplit
					DICO[chaine_split[1].strip()]=chaine_resplit[1]
				#TODO creer un dictionnary avec META_10:'script_ID'             
			#print "DICO", DICO
			tempName=filepath.split(prefix.capitalize())
			filepath="/"+prefix.capitalize()+tempName[1]
			cur.execute("select max(l1_id) from spire_photo_l1")
			MaxL1=cur.fetchone()
			print MaxL1[0]
			if MaxL1[0]!=None:
				L1_id=int(MaxL1[0])+1
			metadata.append(L1_id)
			L1_metadata_names=(DICO['OBS_ID'],DICO['OBJECT'],DICO['OBSERVER'],DICO['PROPOSAL'],DICO['CUSMODE'],DICO['ODNUMBER'],'DATE','DATE-OBS','DATE-END',DICO['ELECSIDE'],DICO['BIASMODE'],DICO["'timeOffset'"],DICO["'timeDrift'"],\
			DICO["'invalidOffsetFlag'"],DICO["'adcErrFlag'"],\
			DICO['PLWBIASA'],DICO['PMWBIASA'],DICO['PSWBIASA'],DICO["'ptcBiasAmpl'"],DICO["'rcRollApp'"],DICO['OBS_MODE'],DICO['AOR'],DICO['EQUINOX'],DICO['RADESYS'],DICO['RA_NOM'],DICO['DEC_NOM'],DICO['RA'],DICO['DEC'],\
			DICO['POSANGLE'],DICO["'BuildVersion'"],DICO["'CalibrationVersion'"],DICO["'scriptID'"],DICO["'fluxConversionDone'"],\
			DICO["'temperatureDriftCorrectionDone'"],DICO["'electricalCrosstalkCorrectionDone'"],DICO["'opticalCrosstalkCorrectionDone'"])
			#print L1_metadata_names
			for name in L1_metadata_names:
				try:
					#print hdr0[name]
					metadata.append(hdr0[name])
				except:
					if (name=='OBS_ID'):
						splitName=filename.split("_")
						obsid=splitName[1].replace("L","")
						obsid=int(obsid,16)
						hdr0.update(name,obsid)
						metadata.append(hdr0[name])
					else:                   
						try:
							metadata.append(DICO2[name])
						except:
							metadata.append("NA")
			#print "metadata",metadata
			metadata.append(urlFilepath(filepath))
			metadata.append(program)
			metadata.append(filename)
			try:
				XYZ=RADECtoXYZ(hdr0['RA'],hdr0['DEC'])
				#print XYZ
			except:
				print "no RA DEC available"
			metadata.append(XYZ[0])
			metadata.append(XYZ[1])
			metadata.append(XYZ[2])
			metadata.append(float(os.path.getsize(str(i))) )
			metadata.append(convert_MegaBytes(os.path.getsize(str(i))) )
			metadata.append(Public)
			metadata.append(prefix)
			metadata.append(processing)
			metadata.append(last)
			print Public
			# check L1 obsid in the database: if not in L1 table,  insert, else update
			#if cur.fetchone()==None:
			cur.execute("insert into SPIRE_Photo_L1 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",metadata)
			#cur.execute("insert into %s values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",metadata)
			print hdr0['OBS_ID'], "inserted in the database"
			hdulist.close()
	


def FillSPIREPhotoL2Table(listfile,cur,prefix,last):
	############ L2 Table
	last_orig=last
	L2_id=0
	L2_obsids_id=0
	metadata_L2=[]
	XYZ=[0.,0.,0.]
	#listR1=["OT2_hdole","DDT_mustdo","SAG-4","OT1_lmontier"]
	for i in listfile:
		if  str(i).count("_notdf")==0 and  str(i).count("test")==0 and str(i).count("old")==0 and str(i).count("HiRes")==0:
			filepath=str(i)
			
			filename=os.path.basename(filepath)
			print filename
			program = filepath.split("/")[-3]
#			if program in listR1 and prefix=="R0_spire_photo":
#				last=False
#			else:
#				last=last_orig
			processing="HIPE"
			if str(i).count("SupremePhoto")==1: processing = "SupremePhoto Plugin"
			processing_L1="HIPE_Sanepic"
			dirpath=os.path.dirname(filepath)
			hdulist=pyfits.open(filepath)
			hdr0=hdulist[0].header
			hdr1=hdulist[1].header
			
			try: crota2=hdr1['CROTA2']
			except: crota2=None
			
			if isPublic(hdr0['DATE-OBS']): Public=True
			else: Public=False
			
			#check if file is already in the database
			cur.execute("SELECT * FROM SPIRE_Photo_L2 WHERE filename like '"+filename+"' and release like '"+prefix+"'" )
			if cur.fetchone()==None:
				###### STEP 1 insert into L2
				if str(i).count("combine")==1: combine=True
				else: combine=False
				
				cur.execute("select max(l2_id) from SPIRE_Photo_L2")
				MaxL2=cur.fetchone()
				if MaxL2[0]!=None:
					L2_id=int(MaxL2[0])+1
					#print L2_id
				tempName=filepath.split(prefix.capitalize())
				filepath="/"+prefix.capitalize()+urlFilepath(tempName[1])
				imagepath=filepath.replace(".fits",".png")
				fullimagepath=filepath.replace(".fits","_fullsize.png")
				
#				try: naxis1, naxis2 = hdr1['NAXIS1'],hdr1['NAXIS2']
#				except: naxis1, naxis2 = hdr0['META_3'], hdr0['META_4']
				cur.execute("SELECT * FROM SPIRE_Photo_L2")
				colnames = [desc[0] for desc in cur.description]
				
				metadata_L2 = [L2_id, hdr0['OBJECT'],filepath,hdr0['OBSERVER'],hdr0['PROPOSAL'],Public,program,processing,filename,imagepath,fullimagepath,\
				hdr1['CRVAL1'],hdr1['CRVAL2'],hdr1['CDELT1'],hdr1['CDELT2'],crota2,hdr1['NAXIS1'],hdr1['NAXIS2'],hdr1['CRPIX1'],hdr1['CRPIX2'],hdr1['CTYPE1'],hdr1['CTYPE2'],\
				hdr0['EQUINOX'],hdr0['WAVELNTH'],hdr0['DESC']]
				
				# add buildVersion from History task
				try: metadata_L2.append(hdulist[6].data[0][3])
				except: metadata_L2.append(None)
				# add XYZ
				try: XYZ=RADECtoXYZ(hdr1['CRVAL1'],hdr1['CRVAL2'])
				except: print "no CRAVL1 CRVAL2 available"
				metadata_L2.append(XYZ[0])
				metadata_L2.append(XYZ[1])
				metadata_L2.append(XYZ[2])
				
				metadata_L2.append(float(os.path.getsize(str(i))) )
				metadata_L2.append( convert_MegaBytes(os.path.getsize(str(i))) )
				metadata_L2.append(prefix)
				metadata_L2.append(combine)
				metadata_L2.append(last)
				
				poly=calculateSpoly(str(i))
				metadata_L2.append(poly)
				
				if str(i).count("combine")==0: obsid = hdr0['OBS_ID']
				else: obsid=999999
				metadata_L2.append(obsid)
				
				###### True for all : will be updated as flase when needed for postprocessing
				level25 = True
				metadata_L2.append(level25)

				#if hdr0['OBJECT'].count("IVCG86")>0 or hdr0['OBJECT'].count("spica")>0: poly=None
				
				#for i in range(len(metadata_L2)): print colnames[i], metadata_L2[i]
				
				cur.execute("insert into SPIRE_Photo_L2 values ("+(len(metadata_L2)-1)*"%s,"+"%s)", metadata_L2)
				print filename, " in the database"
			###### STEP 2 insert into L2_obsids
				if str(i).count("combined")==0:
				#  map with a single obsid
					#cur.execute("select L1_id from SPIRE_Photo_L1 where obsid="+str(hdr0['OBS_ID'])+" and processing like '"+processing_L1+"' and release like '"+prefix+"' and filename not like '%notdf%'")
					#L1_id=cur.fetchone()
					#print "SPIRE HIPE L1_id obsid",L1_id,str(hdr0['OBS_ID'])
					cur.execute("select max(l2_obsids_id) from SPIRE_Photo_L2_obsids")
					MaxL2obsids=cur.fetchone()
					if MaxL2obsids[0]!=None: L2_obsids_id=int(MaxL2obsids[0])+1
					metadata_L2_obsids=(L2_obsids_id,L2_id,hdr0['OBS_ID'])
					cur.execute("insert into SPIRE_Photo_L2_obsids values (%s,%s,%s)",metadata_L2_obsids)
				else:
				# combined map
					if len(hdulist)!=9: 
						print "old combine file, do not insert into the database"
					else:
					# OBSIDS extension exists
						for j in range(len(hdulist["obsids"].data)):
							cur.execute("select max(l2_obsids_id) from SPIRE_Photo_L2_obsids")
							MaxL2obsids=cur.fetchone()
							if MaxL2obsids[0]!=None:
								L2_obsids_id=int(MaxL2obsids[0])+1
							#cur.execute("select L1_id from SPIRE_Photo_L1 where obsid="+str(hdulist["obsids"].data[j][0])+" and processing like '"+processing_L1+"' and release like '"+prefix+"'  and filename not like '%notdf%'")
							#L1_id=cur.fetchone()
							print "SPIRE HIPE combine obsid, ",str(hdulist["obsids"].data[j][0])
							metadata_L2_obsids=(L2_obsids_id,L2_id,int(hdulist["obsids"].data[j][0]))
							cur.execute("insert into SPIRE_Photo_L2_obsids values (%s,%s,%s)",metadata_L2_obsids)
			hdulist.close()


def FillSPIREPhotoDensityTable(listfile,cur,prefix,last):
        buildVersion="12.0.1603"
        ############ L2 Table
        L2_id=0
        metadata_L2=[]
        XYZ=[0.,0.,0.]
        #listR1=["OT2_hdole","DDT_mustdo","SAG-4","OT1_lmontier"]
        for i in listfile:
                if  str(i).count("significance_map")==1 :
                        filepath=str(i)
                        hdulist=pyfits.open(filepath)
                        hdr0=hdulist[0].header
                        program = filepath.split("/")[-3]
                        dirpath=os.path.dirname(filepath)
                        filepath_tar=dirpath+"/"+filepath.split("/")[-2]+"_density.tar"
			print filepath_tar
                        filename=os.path.basename(filepath_tar)

                        try: crota2=hdr0['CROTA2']
                        except: crota2=None

                        #check if file is already in the database
                        cur.execute("SELECT * FROM SPIRE_Photo_Density WHERE filename like '"+filename+"' and release like '"+prefix+"'" )
                        if cur.fetchone()==None:
                                ###### STEP 1 insert into L2
                                cur.execute("select max(l2_id) from SPIRE_Photo_Density")
                                MaxL2=cur.fetchone()
                                if MaxL2[0]!=None:
                                        L2_id=int(MaxL2[0])+1
                                        #print L2_id
                                tempNametar=filepath_tar.split(prefix.capitalize())
                                filepath_tar="/"+prefix.capitalize()+urlFilepath(tempNametar[1])
                                tempName=filepath.split(prefix.capitalize())
                                filepath="/"+prefix.capitalize()+urlFilepath(tempName[1])
                                imagepath=filepath.replace("_significance_map_colorsel_1000akderms.fits","_3color_significance_contour.png")
                                print imagepath
                                cur.execute("SELECT * FROM SPIRE_Photo_Density")
                                colnames = [desc[0] for desc in cur.description]
                                metadata_L2 = [L2_id, hdr0['OBS_ID'],hdr0['OBJECT'],filepath_tar,hdr0['OBSERVER'],hdr0['PROPOSAL'],program,filename,imagepath,\
                                hdr0['CRVAL1'],hdr0['CRVAL2'],hdr0['CDELT1'],hdr0['CDELT2'],crota2,hdr0['NAXIS1'],hdr0['NAXIS2'],hdr0['CRPIX1'],hdr0['CRPIX2'],hdr0['CTYPE1'],hdr0['CTYPE2'],\
                                hdr0['EQUINOX'],hdr0['DESC'],hdr0['MAXDENS'],hdr0['MAXSIG']]
				#print  metadata_L2
                                # add buildVersion Hard Coded (bou...) 
                                metadata_L2.append(buildVersion)
                                # add XYZ
                                try: XYZ=RADECtoXYZ(hdr0['CRVAL1'],hdr0['CRVAL2'])
                                except: print "no CRAVL1 CRVAL2 available"
                                metadata_L2.append(XYZ[0])
                                metadata_L2.append(XYZ[1])
                                metadata_L2.append(XYZ[2])

                                metadata_L2.append(float(os.path.getsize(str(i))) )
                                metadata_L2.append( convert_MegaBytes(os.path.getsize(str(i))) )
                                metadata_L2.append(prefix)
                                metadata_L2.append(last)
                                cur.execute("insert into SPIRE_Photo_Density values ("+(len(metadata_L2)-1)*"%s,"+"%s)", metadata_L2)
                                print filename, " in the database"
                        hdulist.close()

