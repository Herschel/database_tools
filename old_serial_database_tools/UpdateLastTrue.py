#!/usr/bin/python2.7
#
# Small script to show PostgreSQL and Pyscopg together
# update metadata from  SPIRE photo in the Herchel Datatabase (HESIOD)
#

import psycopg2
import pyfits
import glob
import os, string, shutil

old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")
execfile(old_serial_database_tools+"SPIRE_Photo_Tools.py")
execfile(old_serial_database_tools+"SPIRE_FTS_Tools.py")
execfile(old_serial_database_tools+"SPIRE_Catalog_Tools.py")
execfile(old_serial_database_tools+"PACS_Photo_Tools.py")
execfile(old_serial_database_tools+"PACS_Spectro_Tools.py")
execfile(old_serial_database_tools+"SANEPIC_Tools.py")

baseName = "'HID_glx'"
prefix_list_spire_photo=["attic","R0_spire_photo","R1_spire_photo","R2_spire_photo","R3_spire_photo"]
prefix_list_spire_photo=["R5_spire_photo"]

SPIRE_PHOTO_progam_list=["DDT_mustdo_4","DDT_mustdo_5","H-ATLAS","Hi-Gal","OT1_lmontier","OT1_pogle01_1","OT2_hdole","SAG-1","SAG-3","SAG-4","lens_malhotra","OT1_mmiville","GOODS-H"]         

SPIRE_PHOTO_program_not_ready=["DDT_mustdo_4","DDT_mustdo_5","H-ATLAS","Hi-Gal","OT1_lmontier","OT1_pogle01_1","OT2_hdole","SAG-1","lens_malhotra","GOODS-H"]
#List of program which sould be set to False in Release N-1:
ReleaseNMinus1="R4_spire_photo"
#ReleaseNMinus1="R3_spire_fts"
#SPIRE_PHOTO_program_ready=["KPGT_cwilso01","Hi-Gal","SAG-3","SAG-4","OT2_hdole","OT1_lmontier","DDT_mustdo_5","clusters_lowz","OT1_mmiville"]
SPIRE_PHOTO_program_ready=["SAG-3","SAG-4","OT1_mmiville"]
SPIRE_FTS_program_ready=["SAG-4","OT1_atielens"]

#Instrument to process
#traitement = ["PACS_SPECTRO_L1"] 
traitement = ["SPIRE_FTS_L1","SPIRE_FTS_L2"] 
#traitement = ["SPIRE_FTS_L1","SPIRE_FTS_L2","SPIRE_PHOTO_L1","SPIRE_PHOTO_L2"] 
#traitement = ["SPIRE_PHOTO_L1","SPIRE_PHOTO_L2"] 



##### connect to the database
try:
    conn = psycopg2.connect("dbname="+baseName+" user='sitools'")
    #conn = psycopg2.connect("dbname='HID_spectro' user='sitools'")
except:
    print "I am unable to connect to the database"
cur = conn.cursor()


if "SPIRE_PHOTO_L1" in traitement :
	
	listTable=["spire_photo_l1"]
	# SPIRE PHOTO L1 Files (Sanepic)
	#updateToFalse(listTable,cur,ReleaseNMinus1,SPIRE_PHOTO_program_ready)
	updateToTrue(listTable,cur,ReleaseNMinus1,SPIRE_PHOTO_program_not_ready)
	conn.commit()
	
if "SPIRE_PHOTO_L2" in traitement :
	
	listTable = ["spire_photo_l2"]
	# SPIRE PHOTO L2 Files (Maps)
	#updateToTrue(listTable,cur,ReleaseNMinus1,SPIRE_PHOTO_program_not_ready)
	updateToFalse(listTable,cur,ReleaseNMinus1,SPIRE_PHOTO_program_ready)
	conn.commit()
	#createIndexSpoly(listTable,cur)
	###### update Level25 to False for data combined destriped and with Extended Emission Gain
	for prefix in prefix_list_spire_photo:
		print prefix
		basePath_L2_SPIRE_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/MAPS_SPIRE/'
		#motifs = ["destriped","ExtEmi",".fits"]
		motifs = [".fits","Extd"]
		exclure=["notdf","test","old","HiRes"]
		listfile_L2_all = getFilesMotif(basePath_L2_SPIRE_Photo,motifs,exclure)
		#print listfile_L2_all
		updateToLevel25(listTable,listfile_L2_all,prefix,cur)
		conn.commit()

if "PACS_PHOTO_L1" in traitement : listTable = ["pacs_photo_l1"]

if "PACS_PHOTO_L2" in traitement : listTable = ["pacs_photo_l2","pacs_photo_l2_obsids"]

if "SPIRE_FTS_L1" in traitement : 
	listTable = ["spire_fts_l1"]
	# SPIRE PHOTO L1 Files (Sanepic)
	#updateToFalse(listTable,cur,ReleaseNMinus1,SPIRE_PHOTO_program_ready)
	updateToFalse(listTable,cur,ReleaseNMinus1,SPIRE_FTS_program_ready)
	conn.commit()

if "SPIRE_FTS_L2" in traitement : 

	listTable = ["spire_fts_l2"]
        updateToFalse(listTable,cur,ReleaseNMinus1,SPIRE_FTS_program_ready)
        conn.commit()

if "PACS_SPECTRO_L1" in traitement : listTable = ["pacs_spectro_l1"]

if "PACS_SPECTRO_L2" in traitement : listTable = ["pacs_spectro_l2"]

if "PACS_SPECTRO_L25" in traitement :  listTable = ["pacs_spectro_l25"]


cur.close()
conn.close()
